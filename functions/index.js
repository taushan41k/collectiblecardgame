const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

exports.sendBattleNotification = functions.https.onCall((data, context) => 
{
    const roomId = data.roomId;
    const userId = data.userId;

    console.log("Get an invokation for room " + roomId);
    
    const payload = {
        notification: {
          title: 'Let\'s battle!',
          body: 'User is waiting for you to battle.',
          icon: 'https://firebasestorage.googleapis.com/v0/b/ccd-game.appspot.com/o/icon%2Ficon.png?alt=media&token=b00465dc-8ccc-415b-bacc-b382de565900'
        },
        data: {
            roomId: roomId,
            userId: userId
        }
      };
      
    return admin.messaging().sendToTopic('rooms', payload);
})