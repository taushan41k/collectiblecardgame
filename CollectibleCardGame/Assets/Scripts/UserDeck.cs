namespace DefaultNamespace
{
    public class UserDeck
    {
        public string id;
        public string title;
        public string[] cards;
        
        public override string ToString()
        {
            return $"id: {id}\n" +
                   $"title: {title}\n" +
                   $"cards: {string.Join("", cards)}\n";
        }
    }
}