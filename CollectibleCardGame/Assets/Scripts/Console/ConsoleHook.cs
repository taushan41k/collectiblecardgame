using System;
using UnityEngine;

namespace CCG.Common.Console
{
    [CreateAssetMenu(menuName = "Console/Hook")]
    public class ConsoleHook : ScriptableObject
    {
        [NonSerialized] public CardConsole console;

        public void RegisterEvent(string content, Color color = default) => console?.LogEvent(content, color);
    }
}