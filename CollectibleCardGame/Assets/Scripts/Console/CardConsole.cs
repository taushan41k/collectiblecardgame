﻿using CCG.Common.Console;
using TMPro;
using UnityEngine;

public class CardConsole : MonoBehaviour
{
    public ConsoleHook consoleHook;
    
    public Transform console;
    public GameObject textEntryPrefab;

    private TMP_Text[] textPool;
    private int currentIndex;

    public TMP_Text CurrentText => textPool[currentIndex];
    
    private void Awake()
    {
        consoleHook.console = this;
        
        textPool = new TMP_Text[5];

        for (var textIndex = 0; textIndex < textPool.Length; textIndex++)
        {
            var newEntry = Instantiate(textEntryPrefab);
            textPool[textIndex] = newEntry.GetComponent<TMP_Text>();
            textPool[textIndex].transform.SetParent(console);
            textPool[textIndex].gameObject.SetActive(false);
        }
    }

    public void LogEvent(string content, Color color = default)
    {
        currentIndex++;
        if (currentIndex > textPool.Length - 1)
            currentIndex = 0;

        CurrentText.color = color;
        CurrentText.text = content;
        CurrentText.gameObject.SetActive(true);
        CurrentText.transform.SetAsLastSibling();
    }
}
