using System.Linq;

namespace CCG.Common
{
    public class Cheats
    {
        private Game game;

        private int switchCount;
        
        public Cheats(Game game)
        {
            this.game = game;
        }

        public void EndPhase()
        {
            if (game.IsMultiplayer && game.CurrentTurn.player != game.players.First())
                return;
            
            game.CurrentTurn.EndCurrentPhase();
            game.ProcessTurn();
        }

        public void SwitchPlayer()
        {
            var firstPlayer = game.players[(switchCount + 1) % 2];
            var secondPlayer = game.players[switchCount % 2];
            
            game.localPlayerCards.LoadPlayer(firstPlayer);
            game.otherPlayerCards.LoadPlayer(secondPlayer);
            
            game.profiles[0].CreateInfoAboutPlayer(firstPlayer);
            game.profiles[1].CreateInfoAboutPlayer(secondPlayer);
            
            switchCount++;
        }
    }
}