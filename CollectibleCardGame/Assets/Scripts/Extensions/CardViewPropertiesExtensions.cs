using CCG.Cards.Model;
using CCG.Cards.View;

namespace CCG.Extensions
{
    public static class CardViewPropertiesExtensions
    {
        public static void FillWith(this CardViewProperties self, CardProperties cardProperties) => 
            self?.cardElement.FillCardViewProperties(self, cardProperties);
    }
}