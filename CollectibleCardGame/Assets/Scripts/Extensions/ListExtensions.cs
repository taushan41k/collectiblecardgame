using System.Collections.Generic;

namespace CCG.Extensions
{
    public static class ListExtensions
    {
        public static List<T> AddFluently<T>(this List<T> self, T item)
        {
            self.Add(item);
            return self;
        }
    }
}