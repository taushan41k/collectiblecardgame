using System.Numerics;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace CCG.Extensions
{
    public static class TransformExtensions
    {
        public static void Move(this Transform self, Transform to)
        {
            self.SetParent(to);
            self.localPosition = Vector3.zero;
            self.localScale = Vector3.one;
            self.localRotation = Quaternion.identity;
        } 
        
        public static TweenerCore<Vector3, Vector3, VectorOptions> MoveSmoothly(this Transform self, Vector3 to)
        {
            return self.DOMove(to, 0.75f);
        }
    }
}