namespace CCG.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string self) => string.IsNullOrEmpty(self);
    }
}