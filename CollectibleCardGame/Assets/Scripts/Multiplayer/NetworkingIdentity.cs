using System;
using System.Collections.Generic;
using System.Linq;
using CCG.Cards;
using CCG.Cards.Model;
using Photon.Pun;
using UnityEngine;

namespace CCG.Common.Multiplayer
{
    public class NetworkingIdentity : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
    {
        private Multiplayer multiplayer;

        public Player player;
        
        public int ownerId;
        public bool isLocal;

        private readonly Dictionary<int, Card> cards = new Dictionary<int, Card>();
        private readonly List<Card> deckCards = new List<Card>();
        
        public string[] CardIds { get; private set; }

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            multiplayer = Services.Multiplayer;
            
            isLocal = info.photonView.IsMine;
            ownerId = info.photonView.Owner.ActorNumber;

            var data = info.photonView.InstantiationData;
            CardIds = (string[]) data[0];

            multiplayer.AddPlayer(this);
        }

        public Card CardBy(int id)
        {
            cards.TryGetValue(id, out var card);
            return card;
        }

        public void AddCard(Card card)
        {
            Debug.Log($"Trying to add card {card.Title} for player {ownerId} with instanceId {card.instanceId}");
            cards.Add(card.instanceId, card);
            deckCards.Add(card);
            
            player.CreateCard(card);
        }

        public void PickCardFromDeck(Card card)
        {
            var cardGameObject = player.CreateCardController(card);
            player.TakeCardInHand(cardGameObject);
        }

        public void UseCard(Card card)
        {
            var cardToUse = player.handCards.FirstOrDefault(x => x.Title == card.Title);
            player.UseCard(cardToUse);
            card.type.PerformSideEffects(cardToUse);
        }
        
        public void AddAttackingPair(Player enemy, Card playerCard, Card enemyCard)
        {
            var (selectedPlayerCard, selectedEnemyCard) = SelectedPlayerCards(enemy, playerCard, enemyCard);

            player.AddAttackingPair(selectedPlayerCard, selectedEnemyCard);
        }

        public void RemoveAttackingPair(Player enemy, Card playerCard, Card enemyCard)
        {
            var (selectedPlayerCard, selectedEnemyCard) = SelectedPlayerCards(enemy, playerCard, enemyCard);

            player.RemoveAttackingPair(selectedPlayerCard, selectedEnemyCard);
        }

        public void ActivateCardForUse(Card card)
        {
            var cardToActivate = player.tableCards.FirstOrDefault(x => x.Title == card.Title && !x.isReadyToUse);
            if (cardToActivate != null) 
                cardToActivate.isReadyToUse = true;
        }

        private (CardController selectedPlayerCard, CardController selectedEnemyCard) SelectedPlayerCards(Player enemy, Card playerCard, Card enemyCard)
        {
            var selectedPlayerCard = player.tableCards.FirstOrDefault(x => x.Title == playerCard.Title);
            var selectedEnemyCard = enemy.tableCards.FirstOrDefault(x => x.Title == enemyCard.Title);
            
            return (selectedPlayerCard, selectedEnemyCard);
        }
    }
}