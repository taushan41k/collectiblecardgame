using System;
using System.Collections.Generic;
using System.Linq;
using CCG.Cards.Model;
using Photon.Pun;
using Photon.Realtime;
using SO;
using UnityEngine;

namespace CCG.Common.Multiplayer
{
    [RequireComponent(typeof(Session))]
    public class Networking : MonoBehaviourPunCallbacks
    {
        public GameEvent loggerUpdated;
        public GameEvent connected;
        public GameEvent failedToConnected;
        public GameEvent waitingForPlayer;

        public bool isMaster;

        private Resources resources;
        private Matchmaking matchmaking;

        private int cardInstanceId;
        private List<NetworkingPlayer> players = new List<NetworkingPlayer>();

        public StringVariable logger;

        public Session Session { get; set; }

        private void Awake()
        {
            resources = Services.Resources;
            matchmaking = new Matchmaking();
            Session = GetComponent<Session>();

            if (Services.Networking != this)
                Destroy(Services.Networking.gameObject);
            
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            ConnectToServer();
        }

        public override void OnConnectedToMaster()
        {
            logger.value = "Connected!";
            loggerUpdated.Raise();
            connected.Raise();
        }

        public override void OnCreatedRoom()
        {
            isMaster = true;
        }

        public override void OnJoinedRoom()
        {
            var roomName = string.Join(string.Empty, PhotonNetwork.CurrentRoom.Name.ToCharArray().Take(10));
            
            logger.value = $"Waiting for another player... [{roomName}]";
            loggerUpdated.Raise();

            waitingForPlayer.Raise();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            logger.value = $"Disconnected, because {cause}";
            loggerUpdated.Raise();
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            logger.value = $"Failed to join random room, because {message}";
            loggerUpdated.Raise();

            matchmaking.CreateRoom();
        }

        public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
        {
            if (!isMaster)
            {
                logger.value = $"Waiting for start...";
                loggerUpdated.Raise();
                return;
            }
            
            if (PhotonNetwork.PlayerList.Length != Matchmaking.MaxPlayers)
                return;
            
            logger.value = $"Ready to play!";
            loggerUpdated.Raise();

            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.Instantiate("Multiplayer", Vector3.zero, Quaternion.identity);
        }

        public void StartGame()
        {
            matchmaking.JoinRandomRoom();
        }

        private void ConnectToServer()
        {
            PhotonNetwork.AutomaticallySyncScene = false;
            PhotonNetwork.ConnectUsingSettings();

            logger.value = "Connecting...";
            loggerUpdated.Raise();
        }

        public Card CreateCard(string cardId)
        {
            var card = resources.RuntimeCard(cardId);

            cardInstanceId++;
            card.instanceId = cardInstanceId;

            return card;
        }

        public Card CreatedCard(string cardId, int instanceId)
        {
            var card = resources.RuntimeCard(cardId);
            card.instanceId = instanceId;
            return card;
        }

        public Card Card(int instanceId, int ownerId) =>
            NetworkingPlayer(ownerId).Card(instanceId);

        private NetworkingPlayer NetworkingPlayer(int ownerId) =>
            players.First(x => x.ownerId == ownerId);
    }
}