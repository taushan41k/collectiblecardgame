namespace CCG.Common.Multiplayer
{
    public enum CardOperation
    {
        PickCardFromDeck,
        DropCreatureCard,
        ActivateForUse,
    }  
    
    public enum CardsOperation
    {
        MatchCardsForAttack,
        ReturnAttackingCardBack
    }
    
    public enum ValueSyncUp
    {
        PlayerHealth,
        PlayerMana,
        CardHealth,
    }
}