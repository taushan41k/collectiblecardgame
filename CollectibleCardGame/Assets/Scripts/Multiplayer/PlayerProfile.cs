using UnityEngine;

namespace CCG.Common.Multiplayer
{
    [CreateAssetMenu(menuName = "Player Profile")]
    public class PlayerProfile : ScriptableObject
    {
        public string[] cardIds;
    }
}