using System;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;

namespace CCG.Common.Multiplayer
{
    public class Matchmaking
    {
        public const int MaxPlayers = 2;

        private readonly Random random = new Random();

        public void CreateRoom()
        {
            var options = new RoomOptions
            {
                PublishUserId = true,
                CleanupCacheOnLeave = false,
                MaxPlayers = MaxPlayers,
            };

            var roomName = RandomString(256);
            Services.CloudMessaging.SendConnectToRoomNotification(roomName);
            PhotonNetwork.CreateRoom(roomName, options, TypedLobby.Default);
        }

        public void JoinRandomRoom() => PhotonNetwork.JoinRandomRoom();

        private string RandomString(int length)
        {
            const string chars = "wertyuiopasdfghjklzxcvbnm234567890";
            return new string(
                Enumerable.Repeat(chars, length)
                    .Select(x => x[random.Next(x.Length)]).ToArray());
        }

        public void JoinRoom(string roomId) => PhotonNetwork.JoinRoom(roomId);
    }
}