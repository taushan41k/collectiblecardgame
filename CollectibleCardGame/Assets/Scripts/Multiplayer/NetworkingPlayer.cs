using System.Collections.Generic;
using CCG.Cards.Model;

namespace CCG.Common.Multiplayer
{
    public class NetworkingPlayer
    {
        public int ownerId;
        
        private readonly Dictionary<int, Card> cards = new Dictionary<int, Card>();

        public void RegisterCard(Card card) => cards.Add(card.instanceId, card);

        public Card Card(int instanceId)
        {
            cards.TryGetValue(instanceId, out var card);
            return card;
        }
    }
}