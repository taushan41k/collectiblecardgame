using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CCG.Common.Multiplayer
{
    public class Session : MonoBehaviour
    {
        public event Action SceneLoaded;

        public bool Started;

        public void LoadLevel(string level) => StartCoroutine(LoadScene(level));
        
        private IEnumerator LoadScene(string level)
        {
            yield return SceneManager.LoadSceneAsync(level, LoadSceneMode.Single);

            SceneLoaded?.Invoke();
            SceneLoaded = null;

            yield return new WaitForSeconds(1f);
            
            Started = true;
        }
    }
}