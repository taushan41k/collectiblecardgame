using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CCG.Cards;
using CCG.Cards.Model;
using CCG.Common.Core;
using Photon.Pun;
using SO;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace CCG.Common.Multiplayer
{
    public class Multiplayer : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
    {
        public Player localPlayer;
        public Player clientPlayer;

        private PhaseView phaseView;
        
        private Game game;
        private Session session;
        private Networking networking;
        private NetworkingIdentity localPlayerIdentity;

        private Transform identities;

        private readonly List<NetworkingIdentity> players = new List<NetworkingIdentity>(2);

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            networking = Services.Networking;
            session = networking.Session;

            identities = new GameObject("Identities").transform;
            DontDestroyOnLoad(identities.gameObject);

            DontDestroyOnLoad(gameObject);

            InstantiateNetworkingIdentity();

            session.SceneLoaded += () => StartCoroutine(StartGame());

            session.LoadLevel("Game Multiplayer");
        }

        [PunRPC]
        public void InitializeGame(int startingPlayerId)
        {
            game = Services.Game;
            game.InitializeGame(startingPlayerId);
        }

        public void BroadcastTurnEnd(int photonId)
        {
            photonView.RPC("EndTurn", RpcTarget.MasterClient, photonId);
        }

        [PunRPC]
        public void EndTurn(int photonId)
        {
            if (photonId != game.currentPlayer.photonId)
                return;

            if (!networking.isMaster)
                return;
            
            var targetId = game.NextPlayerId();
            photonView.RPC("StartTurn", RpcTarget.All, targetId);
        }

        [PunRPC]
        public void StartTurn(int photonId)
        {
            game.ChangeCurrentTurn(photonId);
        }

        public NetworkingIdentity PlayerBy(int ownerId) => players.FirstOrDefault(x => x.ownerId == ownerId);
        public NetworkingIdentity OpponentFor(int ownerId) => players.FirstOrDefault(x => x.ownerId != ownerId);

        public void BroadcastPlayerWantsToUseCard(int cardId, int photonId, CardOperation operation)
        {
            photonView.RPC("PlayerWantsToUseCard", RpcTarget.MasterClient, cardId, photonId, operation);
        } 
        
        public void BroadcastPlayerWantsToUseCard(int playerCardId, int enemyCardId, int photonId, CardsOperation operation)
        {
            photonView.RPC("PlayerWantsToUseCards", RpcTarget.MasterClient, playerCardId, enemyCardId, photonId, operation);
        }

        public void BroadcastPlayerCreatesCards(int photonId, string[] cardsNames, int[] cardsIds)
        {
            photonView.RPC("PlayerCreatesCards", RpcTarget.All, photonId, cardsNames, cardsIds);
        }

        [PunRPC]
        public void PlayerCreatesCards(int photonId, string[] cardsNames, int[] cardsIds)
        {
            var cards = cardsNames.Zip(cardsIds, (s, i) => new {Name = s, InstanceId = i});
            foreach (var card in cards)
            {
                var createdCard = networking.CreatedCard(card.Name, card.InstanceId);
                PlayerBy(photonId).AddCard(createdCard);
            }
        }

        [PunRPC]
        public void PlayerWantsToUseCard(int cardId, int photonId, CardOperation operation)
        {
            var player = PlayerBy(photonId);
            var card = player.CardBy(cardId);
            if (card != null)
                photonView.RPC("UseCard", RpcTarget.All, cardId, photonId, operation);
        }

        [PunRPC]
        public void PlayerWantsToUseCards(int playerCardId, int enemyCardId, int photonId, CardsOperation operation)
        {
            var player = PlayerBy(photonId);
            var playerCard = player.CardBy(playerCardId);
            
            var enemy = OpponentFor(photonId);
            var enemyCard = enemy.CardBy(enemyCardId);

            if (playerCard != null && enemyCard != null)
                photonView.RPC("UseCards", RpcTarget.All, playerCardId, enemyCardId, photonId, operation);
        }

        [PunRPC]
        public void UseCard(int cardId, int photonId, CardOperation operation)
        {
            var player = PlayerBy(photonId);
            var card = player.CardBy(cardId);
            
            switch (operation)
            {
                case CardOperation.PickCardFromDeck:
                    player.PickCardFromDeck(card);
                    break;
                case CardOperation.DropCreatureCard:
                    player.UseCard(card);
                    break;
                case CardOperation.ActivateForUse:
                    player.ActivateCardForUse(card);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(operation), operation, null);
            }
        }

        [PunRPC]
        public void UseCards(int playerCardId, int enemyCardId, int photonId, CardsOperation operation)
        {
            var player = PlayerBy(photonId);
            var playerCard = player.CardBy(playerCardId);
            
            var enemy = OpponentFor(photonId);
            var enemyCard = enemy.CardBy(enemyCardId);
            
            switch (operation)
            {
                case CardsOperation.MatchCardsForAttack:
                    player.AddAttackingPair(enemy.player, playerCard, enemyCard);
                    break;
                case CardsOperation.ReturnAttackingCardBack:
                    player.RemoveAttackingPair(enemy.player, playerCard, enemyCard);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(operation), operation, null);
            }
        }

        public void PlayerPicksCardFromDeck(Player player)
        {
            var card = player.TakeCardFromDeck();
            if (card == null)
                return;
            
            BroadcastPlayerWantsToUseCard(card.instanceId, player.photonId, CardOperation.PickCardFromDeck);
        }


        public void PlayerChoosesCardsForAttack(Player player, CardController playerCard, CardController enemyCard)
        {
            var selectedPlayerCard = player.tableCards.FirstOrDefault(x => x.Title == playerCard.Title)?.Model;
            if (selectedPlayerCard == null)
                return;

            var enemy = OpponentFor(player.photonId);
            if (enemy == null)
                return;

            var selectedEnemyCard = enemy.player.tableCards.FirstOrDefault(x => x.Title == enemyCard.Title)?.Model;
            if (selectedEnemyCard == null)
                return;
            
            BroadcastPlayerWantsToUseCard(
                selectedPlayerCard.instanceId, selectedEnemyCard.instanceId,
                player.photonId, CardsOperation.MatchCardsForAttack);
        }   
        
        public void PlayerReturnsAttackingCard(Player player, CardController playerCard, CardController enemyCard)
        {
            var selectedPlayerCard = player.tableCards.FirstOrDefault(x => x.Title == playerCard.Title)?.Model;
            if (selectedPlayerCard == null)
                return;

            var enemy = OpponentFor(player.photonId);
            if (enemy == null)
                return;

            var selectedEnemyCard = enemy.player.tableCards.FirstOrDefault(x => x.Title == enemyCard.Title)?.Model;
            if (selectedEnemyCard == null)
                return;
            
            BroadcastPlayerWantsToUseCard(
                selectedPlayerCard.instanceId, selectedEnemyCard.instanceId,
                player.photonId, CardsOperation.ReturnAttackingCardBack);
        }

        public void PlayerUsesCard(Player player, Card cardToUse)
        {
            var card = player.handCards.FirstOrDefault(x => x.Title == cardToUse.Title);
            if (card == null)
                return;
            
            BroadcastPlayerWantsToUseCard(card.Model.instanceId, player.photonId, CardOperation.DropCreatureCard);
        }

        private void InstantiateNetworkingIdentity()
        {
            var profile = UnityEngine.Resources.Load<PlayerProfile>("PlayerProfile");

            profile.cardIds = networking.isMaster ? localPlayer.startingCards : clientPlayer.startingCards;
            
            var data = new object[1];
            data[0] = profile.cardIds;

            PhotonNetwork.Instantiate("NetworkingIdentity", Vector3.zero, Quaternion.identity, data: data);
        }

        public void AddPlayer(NetworkingIdentity identity)
        {
            if (identity.isLocal)
                localPlayerIdentity = identity;

            players.Add(identity);
            identity.transform.parent = identities;
        }

        private IEnumerator StartGame()
        {
            yield return new WaitWhile(() => players.Count != 2);
            
            var playerCardsIds = new Dictionary<int, int[]>();
            var playerCardsNames = new Dictionary<int, string[]>();

            foreach (var identity in players)
            {
                var player = PlayerBy(identity);
                player.BindWith(identity);

                var deckCards = player.currentDeck.Select(x => networking.CreateCard(x)).ToList();
                var cardsNames = deckCards.Select(x => x.name).ToArray();
                var cardsIds = deckCards.Select(x => x.instanceId).ToArray();
                
                playerCardsIds.Add(player.photonId, cardsIds);
                playerCardsNames.Add(player.photonId, cardsNames);
            }

            yield return new WaitForSeconds(2f);

            foreach (var photonId in players.Select(player => player.ownerId))
                BroadcastPlayerCreatesCards(photonId, playerCardsNames[photonId], playerCardsIds[photonId]);

            if (networking.isMaster)
                photonView.RPC("InitializeGame", RpcTarget.All, 1);
        }


        private Player PlayerBy(NetworkingIdentity identity) =>
            identity.isLocal ? localPlayer : clientPlayer;

        public void UpdatePlayerMana(int photonId, int mana)
        {
            photonView.RPC("SyncUpValue", RpcTarget.All, -1, photonId, mana, ValueSyncUp.PlayerMana);
        }

        public void UpdatePlayerHealth(int photonId, int health)
        {
            photonView.RPC("SyncUpValue", RpcTarget.All, -1, photonId, health, ValueSyncUp.PlayerHealth);
        }

        public void UpdateCardHealth(int photonId, int cardId, int health)
        {
            photonView.RPC("SyncUpValue", RpcTarget.All, cardId, photonId, health, ValueSyncUp.CardHealth);
        }

        [PunRPC]
        public void SyncUpValue(int cardId, int photonId, int value, ValueSyncUp operation)
        {
            var player = PlayerBy(photonId);
            var profile = Services.Game.Profile(photonId);

            Card card;
            CardController cardController = null;

            if (cardId != -1)
            {
                card = player.CardBy(cardId);
                cardController = player.player.tableCards.FirstOrDefault(x => x.Title == card.Title);
            }

            switch (operation)
            {
                case ValueSyncUp.PlayerHealth:
                    profile.health.text = value.ToString();
                    break;
                case ValueSyncUp.PlayerMana:
                    profile.mana.text = value.ToString();
                    break;
                case ValueSyncUp.CardHealth:
                    cardController?.UpdateHealth(value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(operation), operation, null);
            }
        }

        public void BroadcastBattleResolution(int playerPhotonId, int enemyPhotonId)
        {
            photonView.RPC("ResolveBattle", RpcTarget.MasterClient, playerPhotonId, enemyPhotonId);
        }

        [PunRPC]
        public void ResolveBattle(int playerPhotonId, int enemyPhotonId)
        {
            var player = PlayerBy(playerPhotonId).player;
            var enemy = PlayerBy(enemyPhotonId).player;
            
            BattleResolver.CalculateSolution(game, player, enemy);
        }
        
        public void BroadcastUpdatePhaseText(string phaseName)
        {
            photonView.RPC("UpdateCurrentPhase", RpcTarget.All, phaseName);
        }
        
        [PunRPC]
        public void UpdateCurrentPhase(string phaseName)
        {
            if (phaseView == null)
                phaseView = FindObjectOfType<PhaseView>();

            phaseView.GetComponent<TMP_Text>().text = phaseName;
        }
    }
}