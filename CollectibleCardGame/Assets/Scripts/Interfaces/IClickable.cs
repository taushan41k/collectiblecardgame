namespace CCG.Common.Interfaces
{
    public interface IClickable
    {
        void Click();
        void Highlight();
    }
}