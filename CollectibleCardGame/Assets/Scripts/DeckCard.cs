﻿using System;
using System.Collections;
using System.Collections.Generic;
using CCG.Common;
using UnityEngine;

public class DeckCard : MonoBehaviour
{
    public Vector3 destination;
    public Player player;

    private Vector3 initialPosition;
    
    private void Awake()
    {
        initialPosition = transform.position;
    }

    public void GoBack()
    {
        transform.position = initialPosition;
    }
}
