using System.Collections.Generic;
using System.Linq;
using CCG.Cards;
using CCG.Cards.View;
using CCG.Common.States;
using CCG.Common.Turns;
using CCG.Common.UI;
using CCG.Extensions;
using DefaultNamespace;
using SO;
using UnityEngine;

namespace CCG.Common
{
    public class Game : MonoBehaviour
    {
        public GameEvent turnChanged;
        public GameEvent phaseChanged;

        public CardsCollection localPlayerCards;
        public CardsCollection otherPlayerCards;

        public Player[] players;
        public PlayerBattleProfile[] profiles;

        public Player currentPlayer;
        
        public Player localPlayer;
        public Player clientPlayer;
        
        public GameObject cardPrefab;

        public StringVariable turnText;
        public int turnIndex;
        public Turn[] turns;

        [SerializeField] private State currentState;
        [SerializeField] private Cheats cheats;

        private Resources resources;

        private bool isInitialized;
        
        public Turn CurrentTurn => Turn(turnIndex);
        public bool IsMultiplayer => Services.Multiplayer != null;
        public Player Enemy => players.First() != currentPlayer ? players.First() : players.Last();

        public State CurrentState
        {
            get => currentState;
            set => currentState = value;
        }

        private void Awake()
        {
            cheats = new Cheats(this);
            resources = Services.Resources;
            
            //todo nt: use DI for all needed services later and don't use this fucking static shit
            //Services.Authentication?.PrintUserInfo();
            
            Services.Game = this;
            
            if (IsMultiplayer)
                return;

            players[0].photonId = 1;
            players[1].photonId = 2;   
            
            players[0].OnEnable();
            players[1].OnEnable();
            
            InitializeGame(1);
        }

        public void InitializeGame(int startingPlayerId)
        {
            AssignPlayers(startingPlayerId);

            PreparePlayers();
        }

        private void AssignPlayers(int startingPlayerId)
        {
            currentPlayer = players.FirstOrDefault(x => x.photonId == startingPlayerId);

            players = new Player[2];
            var matchTurns = new Turn[2];

            for (var index = 0; index < turns.Length; index++)
            {
                players[index] = turns[index].player;

                if (players[index].photonId == startingPlayerId)
                    matchTurns[0] = turns[index];
                else
                    matchTurns[1] = turns[index];
            }

            turns = matchTurns;
        }

        private void PreparePlayers()
        {
            SetUpPlayers();

            turnText.value = CurrentTurn.player.username;
            turnChanged.Raise();

            isInitialized = true;
        }

        private void Update()
        {
            if (!isInitialized)
                return;
            
            ProcessTurn();
            ProcessState();
        } 

        public void ProcessTurn()
        {
            var isComplete = CurrentTurn.Excecute();
            if (!isComplete)
                return;
            
            if (!IsMultiplayer)
            {
                turnIndex++;

                if (turnIndex > turns.Length - 1)
                    turnIndex = 0;

                StartTurn(CurrentTurn, turnIndex);
            }
            else
            {               
               Services.Multiplayer.BroadcastTurnEnd(currentPlayer.photonId);
            }
        }

        public void ChangeCurrentTurn(int photonId)
        {
            var index = PlayerTurnIndex(photonId);
            if (index == -1)
                return;

            var nextTurn = turns[index];

            StartTurn(nextTurn, index);
        }

        private void StartTurn(Turn turn, int index)
        {
            Services.Console.RegisterEvent($"{turn.name} just started!", Color.cyan);
            
            currentPlayer = turn.player;
            turnText.value = turn.player.username;
            turnChanged.Raise();

            if (index == 0)
                turn.RaiseStartEvent();
        }

        public PlayerBattleProfile Profile (int photonId) => profiles.First(x => x.player.photonId == photonId);
        public int NextPlayerId() => NextTurn().player.photonId;

        public Turn NextTurn()
        {
            var currentIndex = turnIndex;
            currentIndex++;
            
            if (currentIndex > turns.Length - 1)
                currentIndex = 0;

            return Turn(currentIndex);
        }

        private Turn Turn(int index) => turns[index];

        private int PlayerTurnIndex(int photonId) =>
            turns
                .Select((x, i) => (found: x.player.photonId == photonId, index: i))
                .ToList()
                .AddFluently((true, -1))
                .First(x => x.Item1)
                .Item2;

        private void ProcessState() => currentState?.Next(Time.deltaTime);

        public void EndPhase() => cheats.EndPhase();
        public void SwitchPlayer() => cheats.SwitchPlayer();

        private void SetUpPlayers()
        {
            for (var playerIndex = 0; playerIndex < players.Length; playerIndex++)
            {
                var player = players[playerIndex];
                var playerProfile = profiles[playerIndex];
                
                if (!IsMultiplayer)
                    player.cardsCollection = player.isHumanPlayer ? localPlayerCards : otherPlayerCards;
                
                player.CreateStartingCards();

                playerProfile.CreateInfoAboutPlayer(player);
            }
        }

        public void PickCardFromDeck()
        {
            if (IsMultiplayer)
                Services.Multiplayer.PlayerPicksCardFromDeck(currentPlayer);
            else
                currentPlayer.TakeCardFromDeckInHand();
        }

        public void CheckGameOver(int health)
        {
            if (health != 0)
                return;

            var result = UnityEngine.Resources.FindObjectsOfTypeAll(typeof(GameResultView)).First() as GameResultView;
            if (result == null)
                return;
                
            result.gameObject.SetActive(true);
            
            if (localPlayer.Health == 0)
                result.loseText.gameObject.SetActive(true);
            else
                result.winText.gameObject.SetActive(true);
        }
    }
}