using System;
using System.Linq;
using CCG.Common.UI;
using DefaultNamespace;

namespace CCG.Core
{
    public class User
    {
        public string id;
        public string avatar;
        public string login;
        public DateTime lastVisitDate;
        public int matchesCount;
        public float winRate;

        public UserMatch[] matches = Array.Empty<UserMatch>();
        public UserDeck[] decks = Array.Empty<UserDeck>();

        public override string ToString()
        {
            return $"id: {id}\n" +
                   $"avatar: {avatar}\n" +
                   $"login: {login}\n" +
                   $"lastVisitDate: {lastVisitDate}\n" +
                   $"matchesCount: {matchesCount}\n" +
                   $"winRate: {winRate}\n" +
                   $"matches:  {string.Join("", matches.Select(x => x.ToString()))}\n" +
                   $"decks:  {string.Join("", decks.Select(x => x.ToString()))}\n";
            ;
        }

        public PlayerInfoViewModel PlayerInfoViewModel()
        {
            return new PlayerInfoViewModel()
            {
                login = login,
                winRate = matches.Length > 0 
                    ? 100f * matches.Count(x => x.result == MatchResult.Win) / matches.Length
                    : 0,
                avatar = Common.Services.CloudStorage.GetSprite(avatar)
            };
        }
    }
}