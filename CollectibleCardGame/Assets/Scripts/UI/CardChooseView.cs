﻿using System.Linq;
using CCG.Cards.Model;
using CCG.Cards.View;
using CCG.Common;
using DefaultNamespace;
using UnityEngine;

public class CardChooseView : MonoBehaviour
{
    public DeckPanelView deckPanelView;
    
    public GameObject mark;
    private UserDeck userDeck;
    private Card card;

    private void Start()
    {
        card = GetComponent<CardView>().card;
        userDeck = Services.CurrentUser.decks.First();
        deckPanelView = FindObjectOfType<DeckPanelView>();
        
        var userHasThisCard = userDeck.cards.Any(y => y == card.id);
        mark.SetActive(userHasThisCard);
    }

    public void SetMark()
    {
        mark.SetActive(!mark.activeSelf);

        if (!mark.activeSelf)
        {
            userDeck.cards = userDeck.cards.Where(x => x != card.id).ToArray();
        }
        else
        {
            var cardsList = userDeck.cards.ToList();
            cardsList.Add(card.id);
            userDeck.cards = cardsList.ToArray();
        }
        
        deckPanelView.UpdateCards(userDeck);
    }
}
