using DefaultNamespace;
using UnityEngine;

public class HistoryEntryViewModel
{
    public Sprite avatar;
    public string opponent;
    public MatchStatus status;
    public MatchResult result;
}