﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CCG.Common.UI
{
    public class PlayerBattleProfile : MonoBehaviour
    {
        public Player player;
        
        public Image avatar;
        public Image usernameBackground;
        
        public TMP_Text health;
        public TMP_Text mana;
        public TMP_Text username;
        
        public void CreateInfoAboutPlayer(Player player)
        {
            player.ClearEvents();
                
            username.text = player.username;
            usernameBackground.color = player.color;
            
            if (player.avatar)
                avatar.sprite = player.avatar;
                
            health.text = player.Health.ToString();
            mana.text = player.Mana.ToString();
            
            player.HealthChanged += UpdateHealth;
            player.HealthChanged += Services.Game.CheckGameOver;
            player.ManaChanged += UpdateMana;
        }

        private void UpdateMana(int mana)
        {
            this.mana.text = mana.ToString();
            if (Services.Game.IsMultiplayer && mana != 10)
                Services.Multiplayer.UpdatePlayerMana(player.photonId, mana);
        }

        private void UpdateHealth(int health)
        {
            this.health.text = health.ToString();
            if (Services.Game.IsMultiplayer && health != 20)
                Services.Multiplayer.UpdatePlayerHealth(player.photonId, health);
        }
    }
}