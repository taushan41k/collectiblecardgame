using System;
using CCG.Extensions;
using TMPro;
using UnityEngine;

namespace CCG.Common.UI
{
    public class LoginFlow : MonoBehaviour
    {
        [SerializeField] private TMP_InputField emailField;
        [SerializeField] private TMP_InputField passwordField;

        public event Action<LoginState> LoginStateChanged;
        public LoginState State { get; private set; }
    
        public string Email => emailField.text;
        public string Password => passwordField.text;

        private void Start()
        {
            emailField.onValueChanged.AddListener(_ => ComputeState());
            passwordField.onValueChanged.AddListener(_ => ComputeState());
        
            ComputeState();
        }

        private void ComputeState()
        {
            if (emailField.text.IsNullOrEmpty())
                ChangeState(LoginState.EnterEmail);
            else if (passwordField.text.IsNullOrEmpty())
                ChangeState(LoginState.EnterPassword);
            else
                ChangeState(LoginState.Ok);
        }

        private void ChangeState(LoginState currentState)
        {
            State = currentState;
            LoginStateChanged?.Invoke(currentState);
        }

        public enum LoginState
        {
            EnterEmail,
            EnterPassword,
            Ok
        }
    }
}