﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CCG.Common;
using CCG.Core;
using UnityEngine;

public class ProfilePanelView : MonoBehaviour
{
    public PlayerInfoView playerInfo;
    public HistoryEntryView[] historyEntries;

    private void Awake()
    {
        var user = Services.CurrentUser;
        Load(user);
    }
    
    public void Load(User user)
    {
        playerInfo.Load(user.PlayerInfoViewModel());

        var userMatches = user.matches.Reverse().ToList();
        for (var historyIndex = 0; historyIndex < userMatches.Count; historyIndex++)
            historyEntries[historyIndex].Load(userMatches[historyIndex].ToHistoryEntryViewModel());
    }
}
