using UnityEngine;
using UnityEngine.UI;

namespace CCG.Common.UI
{
    public class LoginButton : MonoBehaviour
    {
        [SerializeField] private LoginFlow flow;
        [SerializeField] private Button button;

        private void Reset()
        {
            flow = FindObjectOfType<LoginFlow>();
            button = GetComponent<Button>();
        }

        private void Start()
        {
            flow.LoginStateChanged += ChangeButtonState;
            button.onClick.AddListener(LoginUser);
        }

        private void LoginUser() => Services.Authentication.Login(flow.Email, flow.Password);

        private void ChangeButtonState(LoginFlow.LoginState state) => ChangeButtonState();

        private void ChangeButtonState()
        {
            button.interactable = flow.State == LoginFlow.LoginState.Ok;
        }
    }
}