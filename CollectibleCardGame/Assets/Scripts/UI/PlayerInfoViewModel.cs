using UnityEngine;

namespace CCG.Common.UI
{
    public class PlayerInfoViewModel
    {
        public Sprite avatar;
        public string login;
        public float winRate;
    }
}