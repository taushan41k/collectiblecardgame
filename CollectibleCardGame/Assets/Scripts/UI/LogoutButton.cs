﻿using System.Collections;
using System.Collections.Generic;
using CCG.Common;
using UnityEngine;

public class LogoutButton : MonoBehaviour
{
    public void Logout() => Services.Authentication.Logout();
}
