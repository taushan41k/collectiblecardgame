﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HistoryEntryView : MonoBehaviour
{
    public Image avatar;
    public Image avatarFrame;
    public TMP_Text opponent;
    public TMP_Text status;

    private static Color winColor = new Color(0.45f, 0.62f, 0.11f);
    private static Color loseColor = new Color(0.67f, 0.11f, 0.11f);
    
    public void Load(HistoryEntryViewModel model)
    {
        avatar.sprite = model.avatar;
        opponent.text = model.opponent;
        status.text = $"Status: {model.status}\nResult: {model.result}";

        avatarFrame.color = model.result == MatchResult.Win ? winColor : loseColor;
        
        gameObject.SetActive(true);
    }
}