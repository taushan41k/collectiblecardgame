﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CCG.Cards.View;
using CCG.Common;
using CCG.Core;
using DefaultNamespace;
using TMPro;
using UnityEngine;

public class DeckPanelView : MonoBehaviour
{
    public TMP_InputField deckName;
    public GameObject[] cardsInDeck;
    public CardView[] cards;
    
    private User user;

    private void Awake()
    {
        user = Services.CurrentUser;
        var deck = user.decks.First();

        deckName.text = deck.title;

        UpdateCards(deck);
    }

    public void UpdateCards(UserDeck deck)
    {
        foreach (var card in cardsInDeck)
            card.SetActive(false);

        foreach (var card in deck.cards)
        {
            var cardEntry = cardsInDeck.LastOrDefault(x => !x.activeSelf);
            if (cardEntry == null)
                continue;

            var cardUsed = cards.FirstOrDefault(x => x.card.id == card);
            if (cardUsed == null)
                continue;
            
            cardEntry.SetActive(true);
            cardEntry.GetComponentInChildren<TMP_Text>().text = cardUsed.card.Title;
        }
    }

    public void Save()
    {
        var deck = user.decks.First();
        Services.Database.UpdateUsersDeck(user.id, deck.id, JsonUtility.ToJson(deck));
    }
}
