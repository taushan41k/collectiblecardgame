using System.Collections;
using CCG.Services;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CCG.Common.UI
{
    public class AuthenticationFlow : MonoBehaviour
    {
        [SerializeField] private GameObject choicePanel;
        [SerializeField] private GameObject registrationPanel;
        [SerializeField] private GameObject loginPanel;
        
        private IEnumerator Start()
        {
            FirebaseAuthentication.UserRegistered += () => Debug.Log("User registered!!!");
            FirebaseAuthentication.UserRegistered += () => SceneManager.LoadScene("Menu");
            
            FirebaseAuthentication.UserLoggedIn += () => Debug.Log("User logged in!!!");
            FirebaseAuthentication.UserLoggedIn += () => SceneManager.LoadScene("Menu");
            
            FirebaseAuthentication.UserLoggedOut += () => Debug.Log("Bye user!!!");

            var authentication = Services.Authentication;

            yield return new WaitForSeconds(1f);
            
            OpenStartMenu();
        }

        public void OpenRegistrationMenu()
        {
            choicePanel.SetActive(false);
            registrationPanel.SetActive(true);
        }

        public void OpenLoginMenu()
        {
            choicePanel.SetActive(false);
            loginPanel.SetActive(true);
        }

        public void OpenStartMenu()
        {
            choicePanel.SetActive(true);
            registrationPanel.SetActive(false);
            loginPanel.SetActive(false);
        }
    }
}