using UnityEngine;
using UnityEngine.UI;

namespace CCG.Common.UI
{
    public class RegistrationButton : MonoBehaviour
    {
        [SerializeField] private RegistrationFlow flow;
        [SerializeField] private Button button;

        private void Reset()
        {
            flow = FindObjectOfType<RegistrationFlow>();
            button = GetComponent<Button>();
        }

        private void Start()
        {
            flow.RegistrationStateChanged += ChangeButtonState;
            button.onClick.AddListener(RegisterUser);
        }

        private void RegisterUser() => Services.Authentication.Register(flow.Email, flow.Password);

        private void ChangeButtonState(RegistrationFlow.RegistrationState state) => ChangeButtonState();

        private void ChangeButtonState()
        {
            button.interactable = flow.State == RegistrationFlow.RegistrationState.Ok;
        }
    }
}