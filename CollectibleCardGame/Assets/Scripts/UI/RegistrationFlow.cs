﻿using System;
using CCG.Extensions;
using TMPro;
using UnityEngine;

namespace CCG.Common.UI
{
    public class RegistrationFlow : MonoBehaviour
    {
        [SerializeField] private TMP_InputField emailField;
        [SerializeField] private TMP_InputField passwordField;
        [SerializeField] private TMP_InputField verifyField;

        public event Action<RegistrationState> RegistrationStateChanged;
        public RegistrationState State { get; private set; }
    
        public string Email => emailField.text;
        public string Password => passwordField.text;

        private void Start()
        {
            emailField.onValueChanged.AddListener(_ => ComputeState());
            passwordField.onValueChanged.AddListener(_ => ComputeState());
            verifyField.onValueChanged.AddListener(_ => ComputeState());
        
            ComputeState();
        }

        private void ComputeState()
        {
            if (emailField.text.IsNullOrEmpty())
                ChangeState(RegistrationState.EnterEmail);
            else if (passwordField.text.IsNullOrEmpty())
                ChangeState(RegistrationState.EnterPassword);
            else if (passwordField.text != verifyField.text)
                ChangeState(RegistrationState.PasswordsDontMatch);
            else
                ChangeState(RegistrationState.Ok);
        }

        private void ChangeState(RegistrationState currentState)
        {
            State = currentState;
            RegistrationStateChanged?.Invoke(currentState);
        }

        public enum RegistrationState
        {
            EnterEmail,
            EnterPassword,
            PasswordsDontMatch,
            Ok
        }
    }
}
