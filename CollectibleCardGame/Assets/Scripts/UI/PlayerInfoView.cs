﻿using System;
using System.Collections;
using System.Collections.Generic;
using CCG.Common;
using CCG.Common.UI;
using CCG.Core;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoView : MonoBehaviour
{
    public Image avatar;
    public TMP_Text login;
    public TMP_Text winRate;

    public void Load(PlayerInfoViewModel model)
    {
        avatar.sprite = model.avatar;
        login.text = model.login;
        winRate.text = $"Winrate: {model.winRate:0.0}%";
    }
}
