using SO;
using UnityEngine;

namespace CCG.Common
{
    [CreateAssetMenu(menuName = "Card/Cards Collection")]
    public class CardsCollection : ScriptableObject
    {
        public TransformVariable handGrid;
        public TransformVariable tableGrid;

        public void LoadPlayer(Player player)
        {
            foreach (var tableCard in player.tableCards)
                tableCard.Move(to:tableGrid.value);

            foreach (var handCard in player.handCards)
                handCard.Move(to: handGrid.value);
        }
    }
}