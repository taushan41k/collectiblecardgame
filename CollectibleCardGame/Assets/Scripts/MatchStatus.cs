namespace DefaultNamespace
{
    public enum MatchStatus
    {
        Complete,
        Leave
    }
    
    public enum MatchResult
    {
        Win,
        Lose
    }
}