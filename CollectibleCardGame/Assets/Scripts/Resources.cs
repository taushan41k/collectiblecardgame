using System.Collections.Generic;
using CCG.Cards.Model;
using UnityEngine;

namespace CCG.Common
{
    [CreateAssetMenu(menuName = "Common/Resources")]
    public class Resources : ScriptableObject
    {
        public CardElement element;
        public Card[] allCards;

        private readonly Dictionary<string, Card> cards = new Dictionary<string, Card>();
        
        public void Initialize()
        {
            cards.Clear();
            foreach (var card in allCards)
                cards.Add(card.name, card);
        }

        public Card RuntimeCard(string name)
        {
            var card = Card(name);
            if (card == null)
                return null;

            var runtimeCard = Instantiate(card);
            runtimeCard.name = card.name;
            return runtimeCard;
        }  
        
        private Card Card(string name)
        {
            cards.TryGetValue(name, out var card);
            return card;
        }
    }
}