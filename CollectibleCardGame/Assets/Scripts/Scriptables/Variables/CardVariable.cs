﻿using CCG.Cards;
using UnityEngine;

namespace Scriptables.Variables
{
    [CreateAssetMenu(menuName = "Variables/Card Variable")]
    public class CardVariable : ScriptableObject
    {
        [SerializeField] private CardController card;

        public CardController Card
        {
            get => card;
            set => card = value;
        }
    }
}