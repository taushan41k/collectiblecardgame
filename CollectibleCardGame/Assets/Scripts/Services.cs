﻿using CCG.Common.Console;
using CCG.Common.Multiplayer;
using CCG.Core;
using CCG.Services;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Functions;
using Firebase.Storage;
using UnityEngine;

namespace CCG.Common
{
    public class Services
    {
        public static Game Game;

        private static User currentUser;
        
        private static Resources resources;
        private static Networking networking;
        private static Multiplayer.Multiplayer multiplayer;
        private static ConsoleHook consoleHook;

        private static FirebaseAuthentication authentication;
        private static RealtimeDatabase database;
        private static CloudStorage cloudStorage;
        private static CloudMessaging cloudMessaging;

        public static Resources Resources
        {
            get
            {
                if (resources != null)
                    return resources;

                resources = UnityEngine.Resources.Load<Resources>("Resources");
                resources.Initialize();

                return resources;
            }
        }

        public static ConsoleHook Console
        {
            get
            {
                if (consoleHook != null)
                    return consoleHook;

                consoleHook = UnityEngine.Resources.Load<ConsoleHook>("ConsoleHook");

                return consoleHook;
            }
        }

        public static Multiplayer.Multiplayer Multiplayer
        {
            get
            {
                if (multiplayer != null)
                    return multiplayer;

                multiplayer = Object.FindObjectOfType<Multiplayer.Multiplayer>();

                return multiplayer;
            }
        }
        
        public static Networking Networking
        {
            get
            {
                if (networking != null)
                    return networking;

                networking = Object.FindObjectOfType<Networking>();

                return networking;
            }
        }


        public static FirebaseAuthentication Authentication
        {
            get
            {
                if (authentication != null)
                    return authentication;

                if (!FirebaseBootstrap.Initialized)
                    FirebaseBootstrap.Configure();

                FirebaseBootstrap.RegisterCallback(_ =>
                    authentication = new FirebaseAuthentication(FirebaseAuth.DefaultInstance));

                return authentication;
            }
        }

        public static RealtimeDatabase Database
        {
            get
            {
                if (database != null)
                    return database;

#if UNITY_EDITOR
                var firebaseApp = FirebaseApp.Create(FirebaseApp.DefaultInstance.Options, "FIREBASE_EDITOR");

                if (!FirebaseBootstrap.Initialized)
                    FirebaseBootstrap.Configure();

                FirebaseBootstrap.RegisterCallback(_ =>
                    database = new RealtimeDatabase(FirebaseDatabase.GetInstance(firebaseApp)));
#else
                if (!FirebaseBootstrap.Initialized)
                    FirebaseBootstrap.Configure();

                FirebaseBootstrap.RegisterCallback(_ =>
                    database = new RealtimeDatabase(FirebaseDatabase.DefaultInstance));
#endif
                
                return database;
            }
        }
        
        public static CloudStorage CloudStorage
        {
            get
            {
                if (cloudStorage != null)
                    return cloudStorage;
        
                if (!FirebaseBootstrap.Initialized)
                    FirebaseBootstrap.Configure();
        
                FirebaseBootstrap.RegisterCallback(_ =>
                    cloudStorage = new CloudStorage(FirebaseStorage.DefaultInstance));
        
                return cloudStorage;
            }
        }   
        
        public static CloudMessaging CloudMessaging
        {
            get
            {
                if (cloudMessaging != null)
                    return cloudMessaging;
        
                if (!FirebaseBootstrap.Initialized)
                    FirebaseBootstrap.Configure();
        
                FirebaseBootstrap.RegisterCallback(_ => 
                    cloudMessaging = new CloudMessaging(FirebaseFunctions.DefaultInstance));
                
                return cloudMessaging;
            }
        }
        
        public static User CurrentUser
        {
            get
            {
                if (currentUser != null)
                    return currentUser;
        
                if (!FirebaseBootstrap.Initialized)
                    FirebaseBootstrap.Configure();
#if !UNITY_EDITOR
                FirebaseBootstrap.RegisterCallback(_ =>
                    currentUser = Database.FindUser(0.ToString()));
#else
                FirebaseBootstrap.RegisterCallback(_ => 
                    currentUser = Database.FindUser(1.ToString()));
#endif
                return currentUser;
            }
        }
    }
}