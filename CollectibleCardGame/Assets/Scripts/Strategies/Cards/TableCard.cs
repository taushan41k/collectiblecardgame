using CCG.Cards;
using CCG.Common.States;
using CCG.Common.Turns;
using Scriptables.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace CCG.Common.Strategies
{
    [CreateAssetMenu(menuName = "Strategies/Table Card")]
    public class TableCard : CardActionType
    {
        public Phase battlePhase;
        public CardVariable currentCard;
        public CardVariable enemyCard;
        public State choosingEnemyCardState;
        
        public static readonly Color DefaultColor = Color.white;
        public static readonly Color EnemySelectionColor = new Color(1f, 0.63f, 0.63f, 1f);
        public static readonly Color PlayerSelectionColor = new Color(0.6f, 1f, 0.5f, 1f);

        public override void Click(CardController card)
        {
            var game = Services.Game;
            if (game.CurrentTurn.CurrentPhase != battlePhase)
                return;
            
            var playerHasThisCardOnTable = game.currentPlayer.tableCards.Contains(card);
            if (playerHasThisCardOnTable && !card.isReadyToUse)
            {
                Debug.Log("This card is not ready to use!");
                return;
            }
            
            Debug.Log("This card is on my table and ready to attack");

            if (!playerHasThisCardOnTable && enemyCard.Card == null && currentCard.Card != null)
            {
                enemyCard.Card = card;
                card.Highlight(EnemySelectionColor);
            }

            if (!playerHasThisCardOnTable) 
                return;
            
            if (currentCard.Card != null)
                currentCard.Card.Highlight(DefaultColor);
                    
            currentCard.Card = card;
            card.Highlight(PlayerSelectionColor);
        }

        public override void Highlight(CardController card)
        {
            var game = Services.Game;
            if (game.CurrentTurn.CurrentPhase != battlePhase)
                return;

            if (!game.currentPlayer.tableCards.Contains(card))
            {
                Debug.Log("This card is not yours!!!");
                card.Highlight(EnemySelectionColor);
                return;
            }
            else
            {
                Debug.Log("Highlight");
                
                return;
            }
        }
    }
}