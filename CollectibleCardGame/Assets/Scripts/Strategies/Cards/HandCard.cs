using CCG.Cards;
using CCG.Common.States;
using Scriptables.Variables;
using SO;
using UnityEngine;

namespace CCG.Common.Strategies
{
    [CreateAssetMenu(menuName = "Strategies/Hand Card")]
    public class HandCard : CardActionType
    {
        public GameEvent currentCardSelected;
        public CardVariable currentCard;
        public State holdingCardState;
        
        public override void Click(CardController card)
        {
            var game = Services.Game;
            if (!game.currentPlayer.handCards.Contains(card))
                return;
            
            Debug.Log("This card is on my hand");
            currentCard.Card = card;
            Services.Game.CurrentState = holdingCardState;
            currentCardSelected.Raise();
        }

        public override void Highlight(CardController card)
        {
            Debug.Log("Highlight");
        }
    }
}