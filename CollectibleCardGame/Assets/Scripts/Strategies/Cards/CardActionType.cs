using CCG.Cards;
using UnityEngine;

namespace CCG.Common.Strategies
{
    public abstract class CardActionType : ScriptableObject
    {
        public abstract void Click(CardController card);
        public abstract void Highlight(CardController card);
    }
}