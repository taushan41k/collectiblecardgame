using UnityEngine;

namespace CCG.Common.Strategies
{
    public abstract class AreaActionType : ScriptableObject
    {
        public abstract void Execute();
    }
}