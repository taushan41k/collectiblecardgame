using Scriptables.Variables;
using UnityEngine;

namespace CCG.Common.Strategies
{
    [CreateAssetMenu(menuName = "Strategies/Drop Card While Holding")]
    public class DropCard : AreaActionType
    {
        public CardVariable card;
        public CardActionType cardDownAction;

        public override void Execute()
        {
            var cardController = card.Card;
            if (cardController == null)
                return;

            var model = cardController.Model;
            var player = Services.Game.currentPlayer;

            if (player.CanUseCard(model))
                model.type.Play(cardController, cardDownAction);
            else
                Debug.Log($"You don't have enough mana to use {cardController.Title}. " +
                          $"Your mana is {cardController.Mana} and card's cost is {cardController.Mana}");
        }
    }
}