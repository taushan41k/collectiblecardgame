using UnityEngine;

namespace CCG.Common.Strategies.Phases
{
    [CreateAssetMenu(menuName = "Conditions/Battle Phase Start Check")]
    public class BattlePhaseStartCheck : Condition
    {
        public override bool IsValid() => Services.Game.currentPlayer.tableCards.Count > 0;
    }
}