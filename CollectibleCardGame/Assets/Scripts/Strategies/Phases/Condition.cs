using UnityEngine;

namespace CCG.Common.Strategies.Phases
{
    public abstract class Condition : ScriptableObject
    {
        public abstract bool IsValid();
    }
}