using System.Collections.Generic;
using System.Linq;
using CCG.Cards;

namespace CCG.Common.Core
{
    public static class BattleResolver
    {
        public static void CalculateSolution(Game game, Player currentPlayer, Player enemy)
        {
            var attackingCards = currentPlayer.AttackingCards.ToList();
            var attackingPairs = currentPlayer.AttackingPairs;

            if (!enemy.tableCards.Any())
            {
                var playerAttackingCards = attackingPairs.Select(x => x.playerCard);
                var attackingCardsThatCanBeUsed = attackingCards
                    .Where(x => !playerAttackingCards.Contains(x))
                    .ToList();

                if (attackingCardsThatCanBeUsed.Any())
                    AttackEnemy(attackingCardsThatCanBeUsed, enemy);
            }

            foreach (var (playerCard, enemyCard) in attackingPairs)
            {
                playerCard.TakeDamage(enemyCard);
                enemyCard.TakeDamage(playerCard);

                if (!game.IsMultiplayer)
                    continue;

                Services.Multiplayer.UpdateCardHealth(currentPlayer.photonId, playerCard.Model.instanceId, playerCard.Health);
                Services.Multiplayer.UpdateCardHealth(enemy.photonId, enemyCard.Model.instanceId, enemyCard.Health);
            }
        }

        private static void AttackEnemy(IEnumerable<CardController> attackingCards, Player enemy)
        {
            foreach (var card in attackingCards)
                enemy.TakeDamage(card);
        }
    }
}