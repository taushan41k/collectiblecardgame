﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CCG.Common;
using CCG.Common.Multiplayer;
using CCG.Services;
using UnityEngine;
using Resources = UnityEngine.Resources;

public class ServiceBootstrap : MonoBehaviour
{
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        
        var database = Services.Database;

        var cloudStorage = Services.CloudStorage;
        
        if (Services.Networking == null)
            Resources.FindObjectsOfTypeAll<Networking>().First()?.gameObject.SetActive(true);
    }
}
