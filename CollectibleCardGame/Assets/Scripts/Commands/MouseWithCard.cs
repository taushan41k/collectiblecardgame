using CCG.Common.States;
using Scriptables.Variables;
using SO;
using UnityEngine;

namespace CCG.Common.Commands
{
    [CreateAssetMenu(menuName = "Commands/MouseWithCard")]
    public class MouseWithCard : Command
    {
        public CardVariable card;
        public State playerState;
        public GameEvent playerStateChanged;
        
        public override void Execute(float deltaTime)
        {
            var isMouseDown = Input.GetMouseButton(0);
            if (isMouseDown) 
                return;

            var areas = ClickableCommand.CollectResultsOfType<Area>();
            foreach (var area in areas)
                area.DropOn();
            
            card.Card.gameObject.SetActive(true);
            card.Card = null;
            
            Services.Game.CurrentState = playerState;
            playerStateChanged.Raise();
        }
    }
}