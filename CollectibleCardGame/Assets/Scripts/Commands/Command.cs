using UnityEngine;

namespace CCG.Common.Commands
{
   public abstract class Command : ScriptableObject
    {
        public abstract void Execute(float deltaTime);
    }
}