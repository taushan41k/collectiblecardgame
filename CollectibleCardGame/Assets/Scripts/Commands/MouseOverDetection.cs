using UnityEngine;

namespace CCG.Common.Commands
{
    [CreateAssetMenu(menuName = "Commands/MouseOverDetection")]
    public class MouseOverDetection : ClickableCommand
    {
        public override void Execute(float deltaTime)
        {
            if (!Input.GetMouseButton(0))
                return;
            
            base.Execute(deltaTime);
            
            if (!CanExecute)
                return;
            
            CurrentClickable?.Highlight();
        }
    }
}