using System.Linq;
using Scriptables.Variables;
using UnityEngine;

namespace CCG.Common.Commands
{
    [CreateAssetMenu(menuName = "Commands/MouseChoose")]
    public class MouseChoose : ClickableCommand
    {
        public CardVariable playerCard;
        public CardVariable enemyCard;

        public override void Execute(float deltaTime)
        {
            if (!Input.GetMouseButtonDown(0))
                return;

            base.Execute(deltaTime);

            if (!CanExecute)
                return;

            if (Services.Game.currentPlayer.handCards.Contains(CurrentClickable))
                return;
            
            CurrentClickable?.Click();
        }
    }
}