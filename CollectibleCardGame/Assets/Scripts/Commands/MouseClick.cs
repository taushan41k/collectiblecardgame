using UnityEngine;

namespace CCG.Common.Commands
{
    [CreateAssetMenu(menuName = "Commands/MouseClick")]
    public class MouseClick : ClickableCommand
    {
        public override void Execute(float deltaTime)
        {
            if (!Input.GetMouseButtonDown(0))
                return;
            
            base.Execute(deltaTime);
            
            if (!CanExecute)
                return;
            
            CurrentClickable?.Click();
        }
    }
}