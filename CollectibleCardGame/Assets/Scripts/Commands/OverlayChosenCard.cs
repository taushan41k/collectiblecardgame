using Scriptables.Variables;
using UnityEngine;

namespace CCG.Common.Commands
{
    [CreateAssetMenu(menuName = "Commands/OverlayChosenCard")]
    public class OverlayChosenCard : Command
    {
        public CardVariable playerCard;
        public CardVariable enemyCard;

        public override void Execute(float deltaTime)
        {
            if (playerCard.Card == null || enemyCard.Card == null)
                return;

            var game = Services.Game;
            if (game.IsMultiplayer)
                Services.Multiplayer.PlayerChoosesCardsForAttack(game.currentPlayer, playerCard.Card, enemyCard.Card);
            else
                game.currentPlayer.AddAttackingPair(playerCard.Card, enemyCard.Card);

            playerCard.Card = null;
            enemyCard.Card = null;
        }
    }
}