using System.Collections.Generic;
using System.Linq;
using CCG.Common.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CCG.Common.Commands
{
    public class ClickableCommand : Command
    {
        protected IClickable CurrentClickable;
        protected bool CanExecute;

        public override void Execute(float deltaTime)
        {
            CanExecute = false;
            
            var clickables = CollectResultsOfType<IClickable>();
            if (!clickables.Any())
                return;

            CurrentClickable = clickables.First();
            CanExecute = true;
        }

        public static List<T> CollectResultsOfType<T>() where T : class
        {
            var pointerEventData = new PointerEventData(EventSystem.current) {position = Input.mousePosition};
            var results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(pointerEventData, results);

            return results.Select(result => result.gameObject.GetComponentInParent<T>()).Where(x => x != null).ToList();
        }
    }
}