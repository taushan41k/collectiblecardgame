using CCG.Cards.View;
using Scriptables.Variables;
using UnityEngine;

namespace CCG.Common
{
    public class Selected : MonoBehaviour
    {
        public CardView cardView;
        public CardVariable cardVariable;

        private new Transform transform;

        private void Awake()
        {
            transform = GetComponent<Transform>();
            Hide();
        }

        private void Update() => transform.position = Input.mousePosition;

        public void Hide() => cardView.gameObject.SetActive(false);

        public void Load()
        {
            if (cardVariable.Card == null)
                return;

            cardVariable.Card.gameObject.SetActive(false);
            cardView.Load(cardVariable.Card.CardView.card);
            cardView.gameObject.SetActive(true);
        }
    }
}