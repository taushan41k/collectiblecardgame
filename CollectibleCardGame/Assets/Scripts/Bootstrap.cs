using System;
using System.Collections;
using CCG.Common.UI;
using CCG.Services;
//using Firebase;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CCG.Common
{
    public class Bootstrap : MonoBehaviour
    {
        [SerializeField] private AuthenticationFlow authenticationFlow;
        
        private IEnumerator Start()
        {
            Debug.Log($"FirebaseBootstrap.Configure at {DateTime.Now.Millisecond}");
            
            FirebaseBootstrap.Configure();

            yield return new WaitUntil(() => !FirebaseBootstrap.Initialized);
            yield return new WaitForSeconds(1f);

            yield return null;
            
            Debug.Log($"Configured at {DateTime.Now.Millisecond}");

            authenticationFlow.gameObject.SetActive(true);

            //SceneManager.LoadScene("Menu");
        }
    }
}