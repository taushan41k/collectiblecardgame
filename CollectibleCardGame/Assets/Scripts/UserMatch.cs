using System;
using DefaultNamespace;

namespace CCG.Core
{
    public class UserMatch
    {
        public string id;
        public string opponentId;
        public DateTime endDate = DateTime.Now;
        public MatchResult result;
        public MatchStatus status;

        public override string ToString()
        {
            return $"id: {id}\n" +
                   $"opponentId: {opponentId}\n" +
                   $"endDate: {endDate}\n" +
                   $"result: {result}\n" +
                   $"status: {status}\n";
        }

        public HistoryEntryViewModel ToHistoryEntryViewModel()
        {
            var opponent = Common.Services.Database.FindUser(opponentId);
            return new HistoryEntryViewModel()
            {
                status = status,
                result = result,
                opponent = opponent?.login ?? "Unknown user",
                avatar = Common.Services.CloudStorage.GetSprite(opponent?.avatar ?? "avatar_6")
            };
        }
    }
}