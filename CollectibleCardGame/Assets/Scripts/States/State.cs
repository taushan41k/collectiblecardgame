using CCG.Common.Commands;
using UnityEngine;

namespace CCG.Common.States
{
    [CreateAssetMenu(menuName = "State")]
    public class State : ScriptableObject
    {
        public Command[] commands;

        public void Next(float deltaTime)
        {
            foreach (var command in commands)
                command.Execute(deltaTime);
        }
    }
}