using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CCG.Cards;
using CCG.Cards.Model;
using CCG.Cards.View;
using CCG.Common.Multiplayer;
using CCG.Common.Strategies;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace CCG.Common
{
    [CreateAssetMenu(menuName = "Players/Player")]
    public class Player : ScriptableObject
    {
        public event Action<int> HealthChanged;
        public event Action<int> ManaChanged;

        [NonSerialized] public int photonId = -1;

        public string username;
        public Color color;
        public string[] startingCards;

        public List<string> currentDeck = new List<string>();

        public bool isHumanPlayer;

        public Sprite avatar;
        [SerializeField] private int mana;
        [SerializeField] private int health;

        private NetworkingIdentity networkingIdentity;

        public CardsCollection cardsCollection;

        public CardActionType handActionType;
        public CardActionType tableActionType;

        [NonSerialized] public List<CardController> handCards = new List<CardController>();
        [NonSerialized] public List<CardController> tableCards = new List<CardController>();

        [NonSerialized] public List<Card> allCards = new List<Card>();
        [NonSerialized] public List<CardController> deck = new List<CardController>();

        public IEnumerable<CardController> AttackingCards => tableCards.Where(x => x.isReadyToUse);

        private DeckCard deckCard;

        public int Mana
        {
            get => mana;
            private set
            {
                mana = value;

                ManaChanged?.Invoke(mana);
            }
        }

        public int Health
        {
            get => health;
            private set
            {
                health = value;
                if (health < 0)
                    health = 0;

                HealthChanged?.Invoke(health);
            }
        }

        public List<(CardController playerCard, CardController enemyCard)> AttackingPairs { get; } =
            new List<(CardController playerCard, CardController enemyCard)>();

        public void OnEnable()
        {
            health = 20;
            mana = 10;
        }

        public bool CanUseCard(Card card) => Mana >= card.Mana;

        public void UseCard(CardController card)
        {
            if (!CanUseCard(card.Model))
                return;

            Services.Console.RegisterEvent(
                $"Player {username} with mana {mana} used card {card.Title} for cost {card.Mana}", color);
            Mana -= card.Mana;
            Services.Console.RegisterEvent($"Player {username} has {mana} mana after use", color);

            if (handCards.Contains(card))
                handCards.Remove(card);
        }

        public void TakeDamage(CardController card)
        {
            Health -= card.Attack;
            Services.Console.RegisterEvent($"Player {username} took damage {card.Attack} from {card.Title}", color);
        }

        public void ClearEvents()
        {
            ManaChanged = null;
            HealthChanged = null;
        }

        public void BindWith(NetworkingIdentity identity)
        {
            var game = Services.Game;
            identity.player = identity.isLocal ? game.localPlayer : game.clientPlayer;

            cardsCollection = identity.isLocal ? game.localPlayerCards : game.otherPlayerCards;
            photonId = identity.ownerId;
            networkingIdentity = identity;
            
            currentDeck.Clear();
            currentDeck.AddRange(identity.CardIds);
        }

        public void CreateStartingCards()
        {
            if (Services.Game.IsMultiplayer)
                return;

            foreach (var cardName in startingCards)
            {
                var card = Services.Resources.RuntimeCard(cardName);

                CreateCard(card);

                CreateCardController(card);
            }

            Services.Console.RegisterEvent($"Created cards for player {username}", color);
        }

        public CardController TakeCardFromDeckInHand()
        {
            var card = TakeCardFromDeck();
            if (card == null)
                return null;

            var cardController = TakeCardController(card);
            if (cardController == null)
                return null;

            TakeCardInHand(cardController.gameObject);

            return cardController;
        }

        public Card TakeCardFromDeck()
        {
            var random = new System.Random();
            var card = allCards.OrderBy(x => random.Next()).FirstOrDefault();
            if (card == null)
                return null;

            allCards.RemoveAt(0);
            return card;
        }

        public void TakeCardInHand(GameObject cardGameObject)
        {
            var cardController = cardGameObject.GetComponent<CardController>();
            cardController.currentCardAction = handActionType;

            if (deckCard == null)
                deckCard = FindObjectsOfType<DeckCard>().First(x => x.player == this);

            deckCard.transform.DOLocalMove(deckCard.destination, 0.8f, true)
                .OnComplete(() =>
                {
                    MoveToHand(cardController);
                    deckCard.GoBack();
                });
        }

        private void MoveToHand(CardController cardController)
        {
            cardController.Move(to: cardsCollection.handGrid.value);

            Services.Console.RegisterEvent(
                $"Player {username} takes {cardController.Title} from deck and moves to {cardsCollection.name}", color);

            handCards.Add(cardController);
        }

        //todo: create Deck entity for DDD
        public GameObject CreateCardController(Card card)
        {
            var cardGameObject = Instantiate(Services.Game.cardPrefab);

            var cardView = cardGameObject.GetComponent<CardView>();
            cardView.Load(card);

            var cardController = cardGameObject.GetComponent<CardController>();
            cardController.Initialize();
            deck.Add(cardController);

            return cardGameObject;
        }

        public void CreateCard(Card card) => allCards.Add(card);

        public CardController TakeCardController(Card card)
        {
            var cardController = deck.FirstOrDefault(x => x.Title == card.Title);
            if (cardController != null)
                deck.Remove(cardController);

            return cardController;
        }

        public void AddAttackingPair(CardController playerCard, CardController enemyCard)
        {
            AttackingPairs.Add((playerCard, enemyCard));
            
            playerCard.RememberPosition();

            var enemyPosition = enemyCard.transform.position;
            enemyPosition += new Vector3(0.05f, 0.05f, 0.05f);
            
            playerCard.MoveSmoothly(enemyPosition);
        }

        public void RemoveAttackingPair(CardController playerCard, CardController enemyCard)
        {
            AttackingPairs.Remove((playerCard, enemyCard));

            var tweener = playerCard.GoBack();
            
            TryDestroyCards(playerCard, enemyCard, tweener);
        }

        private void TryDestroyCards(CardController playerCard, CardController enemyCard, TweenerCore<Vector3, Vector3, VectorOptions> tweener)
        {
            tweener.OnComplete(() =>
            {
                if (playerCard.Health == 0)
                    DoPunchScale(playerCard)
                        .OnComplete(() =>
                        {
                            tableCards.Remove(playerCard);
                            Destroy(playerCard.gameObject);
                        });

                if (enemyCard.Health == 0)
                    DoPunchScale(enemyCard)
                        .OnComplete(() =>
                        {
                            var game = Services.Game;    
                            if (game.localPlayer.tableCards == tableCards)
                                game.clientPlayer.tableCards.Remove(enemyCard);
                            else
                                game.localPlayer.tableCards.Remove(enemyCard);

                            Destroy(enemyCard.gameObject);
                        });
            });

            Tweener DoPunchScale(Component card) => 
                card.transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0.1f), 0.5f);
        }
        public void ResetMana()
        {
            Mana = 10;
        }
    }
}