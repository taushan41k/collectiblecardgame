using System;
using CCG.Common;
using CCG.Core;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class GameResultView : MonoBehaviour
    {
        public TMP_Text winText;
        public TMP_Text loseText;
        public Button okButton;
        public void Ok()
        {
            if (Services.Game.localPlayer.Health == 0)
            {
                var match = new UserMatch()
                {
                    opponentId = 2.ToString(),
                    endDate = DateTime.Today,
                    id = Guid.NewGuid().ToString(),
                    result = MatchResult.Lose,
                    status = MatchStatus.Complete
                };
                
                Services.Database?.UpdateMatch(Services.CurrentUser.id, JsonUtility.ToJson(match));
            }
            else
            {
                var match = new UserMatch()
                {
                    opponentId = 2.ToString(),
                    endDate = DateTime.Today,
                    id = Guid.NewGuid().ToString(),
                    result = MatchResult.Win,
                    status = MatchStatus.Complete
                };
                
                Services.Database?.UpdateMatch(Services.CurrentUser.id, JsonUtility.ToJson(match));
            }

            SceneManager.LoadScene("Menu");
        }
    }
}