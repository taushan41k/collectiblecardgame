using CCG.Common.Strategies;
using UnityEngine;

namespace CCG.Common
{
    public class Area : MonoBehaviour
    {
        public AreaActionType currentAreaAction;
        
        public void DropOn() => currentAreaAction.Execute();
    }
}