using CCG.Cards.View;
using UnityEngine;

namespace CCG.Cards.Model
{
    public abstract class CardElement : ScriptableObject
    {
        public abstract void FillCardViewProperties(CardViewProperties viewProperties, CardProperties cardProperty);
    }
}