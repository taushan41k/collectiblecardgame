using CCG.Cards.View;
using UnityEngine;

namespace CCG.Cards.Model
{    
    [CreateAssetMenu(menuName = "Elements/Int")]

    public class IntElement : CardElement
    {
        public override void FillCardViewProperties(CardViewProperties viewProperties, CardProperties cardProperty) => 
            viewProperties.text.text = cardProperty.number.ToString();
    }
}