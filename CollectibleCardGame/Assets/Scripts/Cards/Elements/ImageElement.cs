using CCG.Cards.View;
using UnityEngine;

namespace CCG.Cards.Model
{
    [CreateAssetMenu(menuName = "Elements/Image")]
    public class ImageElement : CardElement
    {
        public override void FillCardViewProperties(CardViewProperties viewProperties, CardProperties cardProperty) => 
            viewProperties.image.sprite = cardProperty.sprite;
    }
}