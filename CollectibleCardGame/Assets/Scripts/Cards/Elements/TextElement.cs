using CCG.Cards.View;
using UnityEngine;

namespace CCG.Cards.Model
{    
    [CreateAssetMenu(menuName = "Elements/Text")]
    public class TextElement : CardElement
    {
        public override void FillCardViewProperties(CardViewProperties viewProperties, CardProperties cardProperty) => 
            viewProperties.text.text = cardProperty.text;
    }
}