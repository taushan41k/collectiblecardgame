using System;
using CCG.Cards.Model;
using UnityEngine.UI;

namespace CCG.Cards.View
{
    [Serializable]
    public class CardViewProperties
    {
        public Text text;
        public Image image;
        public CardElement cardElement;
    }
}