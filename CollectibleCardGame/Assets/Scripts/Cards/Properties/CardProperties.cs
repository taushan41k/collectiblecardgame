using System;
using UnityEngine;

namespace CCG.Cards.Model
{
    [Serializable]
    public class CardProperties
    {
        public string text;
        public int number;
        public Sprite sprite;
        public CardElement cardElement;

        public CardProperties(string text, int number = 0, Sprite sprite = null, CardElement cardElement = null)
        {
            this.text = text;
            this.number = number;
            this.sprite = sprite;
            this.cardElement = cardElement;
        }

        public override string ToString() =>
            $"{cardElement.name} : " +
            $"{(sprite == null ? text : "")}" +
            $"{(number != 0 ? $" : {number.ToString()}" : $"{(sprite == null ? "" : sprite.ToString())}")}\n";
    }
}