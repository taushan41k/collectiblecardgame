using System;

namespace CCG.Cards.Model
{
    public class CardDto
    {
        [NonSerialized]
        public string id;
        
        public string title;
        public string description;
        public string category;
        public string image;
        
        public int attack;
        public int cost;
        public int health;

        public CardDto(string title, string description, string category, string image, int attack, int cost, int health)
        {
            this.title = title;
            this.description = description;
            this.category = category;
            this.image = image;
            this.attack = attack;
            this.cost = cost;
            this.health = health;
        }
    }
}