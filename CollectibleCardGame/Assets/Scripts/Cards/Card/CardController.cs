using CCG.Cards.Model;
using CCG.Cards.View;
using CCG.Common.Interfaces;
using CCG.Common.Strategies;
using CCG.Extensions;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

namespace CCG.Cards
{
    public class CardController : MonoBehaviour, IClickable
    {
        public CardView CardView { get; private set; }
        public Card Model => CardView.card;
        public int Health
        {
            get => health;
            private set
            {
                health = value;
                if (health < 0)
                    health = 0;
                
                healthView.text = health.ToString();
            }
        }

        public int Mana => Model.Mana;
        public int Attack => Model.Attack;
        public string Title => Model.Title;

        public double Rate => (Attack + Health) / ( Mana == 0 ? 1f : Mana);
        
        public CardActionType currentCardAction;
        public bool isReadyToUse;

        private int health;
        private Text healthView;
        
        private Vector3 initialPosition;
        
        private void Start() => Initialize();

        public void Initialize()
        {
            CardView = GetComponent<CardView>();
            healthView = CardView.statistics.transform.Find("Health").GetComponent<Text>();
            health = Model.Health;
        }

        // ReSharper disable once Unity.NoNullPropagation
        public void Click() => currentCardAction?.Click(this);

        // ReSharper disable once Unity.NoNullPropagation
        public void Highlight() => currentCardAction?.Highlight(this);

        public void Move(Transform to) => transform.Move(to);
        public void MoveSmoothly(Vector3 to) => transform.MoveSmoothly(to);
        
        public void Highlight(Color color)
        {
            var cardBackground = GetComponentInChildren<CardBackground>();
            var image = cardBackground.GetComponent<Image>();
            image.color = color;
        }
        
        public void TakeDamage(CardController card)
        {
            Health -= card.Attack;
            Common.Services.Console.RegisterEvent($"Card {Title} took damage {card.Attack} from {card.Title}", Color.yellow);
            Common.Services.Console.RegisterEvent($"Card {Title} health is {Health}", Color.yellow);
        }

        public void UpdateHealth(int value) => Health = value;
    
        public void RememberPosition()
        {
            initialPosition = transform.position;
        }

        public TweenerCore<Vector3, Vector3, VectorOptions> GoBack()
        {
            return transform.MoveSmoothly(initialPosition);
        }
    }
}