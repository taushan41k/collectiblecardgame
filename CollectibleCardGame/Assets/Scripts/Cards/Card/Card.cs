﻿using System;
using System.Linq;
using CCG.Extensions;
using UnityEditor;
using UnityEngine;

namespace CCG.Cards.Model
{
    [CreateAssetMenu(menuName = "Card")]
    public class Card : ScriptableObject
    {
        [NonSerialized] public int instanceId;
        
        public string id;
        public CardType type;
        public CardProperties[] properties;

        public string Category => type.name;
        public string Title => properties[0].text;
        public string Description => properties[1].text;
        public string Art => properties[2].sprite.name;
        public int Mana => properties[3].number;
        public int Health => properties[4].number;
        public int Attack => properties[5].number;

#if UNITY_EDITOR
        private void Reset() => OnValidate();
        
        private void OnValidate()
        {
            id = id.IsNullOrEmpty() ? Guid.NewGuid().ToString() : id;
            if (properties == null || properties.Length != 6)
                properties = new[]
                {
                    new CardProperties("Title", cardElement: AssetDatabase.LoadAssetAtPath<TextElement>("Assets/Data/Elements/Texts/Title.asset")),
                    new CardProperties("Description", cardElement: AssetDatabase.LoadAssetAtPath<TextElement>("Assets/Data/Elements/Texts/Description.asset")),
                    new CardProperties("Art", cardElement: AssetDatabase.LoadAssetAtPath<ImageElement>("Assets/Data/Elements/Images/Art.asset")),
                    new CardProperties("Mana", cardElement: AssetDatabase.LoadAssetAtPath<IntElement>("Assets/Data/Elements/Ints/Mana.asset")),
                    new CardProperties("Health", cardElement: AssetDatabase.LoadAssetAtPath<IntElement>("Assets/Data/Elements/Ints/Health.asset")),
                    new CardProperties("Attack", cardElement: AssetDatabase.LoadAssetAtPath<IntElement>("Assets/Data/Elements/Ints/Attack.asset"))
                };
        } 
#endif
        
        public CardDto ToDto() => new CardDto(Title, Description, Category, Art, Attack, Mana, Health);

        public override string ToString() => 
            $"Type: {type.name}\n" +
            "==Properties===\n" +
            $"{string.Join("", properties.Select(x => x.ToString()))}";
    }
}