using System;
using System.Linq;
using CCG.Cards.Model;
using CCG.Extensions;
using UnityEngine;

namespace CCG.Cards.View
{
    public class CardView : MonoBehaviour
    {
        public Card card;
        public CardViewProperties[] properties;
        
        public GameObject statistics;
        public GameObject raycastTarget;
        
        private void Start()
        {
            Load(card);
        }

        public void Load(Card card)
        {
            if (card == null)
                throw new NullReferenceException();

            this.card = card;

            card.type.SetType(this);
            
            foreach (var cardProperties in card.properties)
            {
                var viewProperties = ViewProperties(cardProperties.cardElement);
                viewProperties.FillWith(cardProperties);
            }  
        }

        public CardViewProperties ViewProperties(CardElement cardElement) =>
            properties.First(x => x.cardElement == cardElement);
    }
}