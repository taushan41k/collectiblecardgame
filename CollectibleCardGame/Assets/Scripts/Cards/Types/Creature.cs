using CCG.Cards.View;
using CCG.Common.Strategies;
using UnityEngine;

namespace CCG.Cards
{
    [CreateAssetMenu(menuName = "Cards/Creature")]
    public class Creature : CardType
    {
        public CardActionType action;
        
        public override void SetType(CardView view)
        {
            base.SetType(view);
            view.statistics.SetActive(true);
        }

        public override void Play(CardController card, CardActionType nextAction)
        {
            base.Play(card, nextAction ? nextAction : action);

            PerformSideEffects(card);
        }

        public override void PerformSideEffects(CardController card)
        {
            var game = Common.Services.Game;
            var currentPlayer = game.currentPlayer;
            
            card.Move(to: currentPlayer.cardsCollection.tableGrid.value);
            card.currentCardAction = action;

            if (!currentPlayer.tableCards.Contains(card))
                currentPlayer.tableCards.Add(card);
        }
    }
}