using CCG.Cards.View;
using CCG.Common.Strategies;
using UnityEngine;

namespace CCG.Cards
{  
    [CreateAssetMenu(menuName = "Cards/Spell")]
    public class Spell : CardType
    {
        public override void SetType(CardView view)
        {
            base.SetType(view);
            view.statistics.transform.Find("Health").gameObject.SetActive(false);
        }

        public override void Play(CardController card, CardActionType nextAction)
        {
            base.Play(card, nextAction);

            PerformSideEffects(card);
        }

        public override void PerformSideEffects(CardController card)
        {
            Common.Services.Game.Enemy.TakeDamage(card);
            
            Common.Services.Console.RegisterEvent("Cast spell!", Color.red);
            Destroy(card.gameObject);
        }
    }
}