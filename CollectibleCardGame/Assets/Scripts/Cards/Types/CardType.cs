﻿using CCG.Cards.View;
using CCG.Common.Strategies;
using UnityEngine;

namespace CCG.Cards
{
    public abstract class CardType : ScriptableObject
    {
        public string name;
        
        public virtual void SetType(CardView view)
        {
            var typeElement = Common.Services.Resources.element;
            var viewProperty = view.ViewProperties(typeElement);
            viewProperty.text.text = name;
        }

        public virtual void Play(CardController card, CardActionType nextAction)
        {
            var game = Common.Services.Game;
            if (!game.IsMultiplayer)
                game.currentPlayer.UseCard(card);
            else
                Common.Services.Multiplayer.PlayerUsesCard(game.currentPlayer, card.Model);
        }

        public abstract void PerformSideEffects(CardController card);
    }
}