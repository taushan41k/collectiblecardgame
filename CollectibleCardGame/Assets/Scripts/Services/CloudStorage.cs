using System;
using System.IO;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Firebase.Storage;
using System.Threading.Tasks;
using Firebase.Extensions;

namespace CCG.Services
{
    public class CloudStorage
    {
        public const string ServerUrl = "gs://ccd-game.appspot.com";
        
        public static readonly ConcurrentDictionary<string, Sprite> Cache = new ConcurrentDictionary<string, Sprite>();
        private static int DownloadingFilesAmount { get; set; }

        private static string cachedResourcesPath;

        private FirebaseStorage storage;
        private StorageReference rootReference;
        private StorageReference avatarsReference;
        
        public CloudStorage(FirebaseStorage storage)
        {
#if UNITY_EDITOR
            cachedResourcesPath = $"{Application.streamingAssetsPath}/";
#else
            cachedResourcesPath = $"{Application.persistentDataPath}/";
#endif

            this.storage = storage;
            rootReference = storage.GetReferenceFromUrl(ServerUrl);
            avatarsReference = rootReference.Child("avatars");
            
            CacheExistingSprites();

            var tasks = Enumerable.Range(1, 6)
                .Select(index => $"avatar_{index}")
                .Select(DownloadFileSmart)
                .ToArray();
        }

        private void CacheExistingSprites()
        {
            if (!Directory.Exists(cachedResourcesPath))
                return;

            foreach (var texture in Directory.GetFiles(cachedResourcesPath).Where(path => !path.Contains(".meta")))
                TryCacheSprite(Path.GetFileName(texture), File.ReadAllBytes(texture));
        }

        public Sprite GetSprite(string name)
        {
            var localPath = $"{cachedResourcesPath}{name}.png";
            Debug.Log("Getting sprite when cache is " + Cache.Count);
            return Cache.TryGetValue(localPath, out var result)
                ? result
                : CreateSprite(CreateTexture(File.ReadAllBytes($"{localPath}")));
        }

        public async Task DownloadFileSmart(string name)
        {
            var serverPath = $"{ServerUrl}{avatarsReference.Path}/{name}.png";
            var localPath = $"{cachedResourcesPath}{name}.png";
            
            Debug.Log($"DownloadFileSmart from  {serverPath} to  {localPath}");

            var fileReference = storage.GetReferenceFromUrl(serverPath);
            storage.MaxDownloadRetryTime = TimeSpan.FromSeconds(5);

            var meta = await CheckMeta(fileReference);
            if (meta == null) 
                return;

            var file = new FileInfo(localPath);

#if UNITY_EDITOR
            Debug.Log("Before checking");
            Debug.Log($"File.Exists(localPath): {File.Exists(localPath)}");
            Debug.Log($"meta.SizeBytes: {meta.SizeBytes}");
            Debug.Log($"file: {file}");
#endif

            try
            {
                if (!File.Exists($"{localPath}") || meta.SizeBytes != file.Length)
                {
                    Debug.Log("GetFileAsync hard");
                    await DownloadFileHard(serverPath, localPath);
                }
                else
                {
                    TryCacheSprite(localPath);
                    Debug.Log("Already exists and the size is actual");
                }
            }
            catch (Exception e)
            {
                Debug.Log("ERROR EXISTS: " + e);
            }

            Debug.Log("DownloadFileSmart - Finish");
        }

        private static async Task<StorageMetadata> CheckMeta(StorageReference fileReference)
        {
            StorageMetadata meta;
            
            try
            {
                meta = await fileReference.GetMetadataAsync();
                if (meta == null)
                {
                    Debug.Log("Meta == null");
                    return null;
                }
            }
            catch (Exception e)
            {
                switch (Convert.ToInt32(e.Data["code"]))
                {
                    case 404:
                        Debug.Log("FirebaseStorage: File not found: " + fileReference.Path);
                        break;
                    case 403:
                        Debug.Log("FirebaseStorage: permission denied to file: " + fileReference.Path);
                        break;
                    default:
                        Debug.Log("FirebaseStorage: unknown critical error with file: " + fileReference.Path + "   Error: " +
                                  e);
                        break;
                }

                return null;
            }

            return meta;
        }

        public async Task DownloadFileHard(string serverPath, string localPath)
        {
            Debug.Log("DownloadFileHard - Start");
            var fileRef = storage.GetReferenceFromUrl(serverPath);
            
            Debug.Log("DownloadFileHard - PathServer: " + serverPath);

            DownloadingFilesAmount++;
            
            var task = fileRef.GetFileAsync("file://" + localPath);
            await task.ContinueWithOnMainThread(t =>
            {
                if (!t.IsFaulted && !t.IsCanceled)
                    TryCacheSprite(localPath);
            });

            DownloadingFilesAmount--;
            Debug.Log("DownloadFileHard - Downloaded " + localPath);
        }

        private void TryCacheSprite(string localPath)
        {
            if (Cache.ContainsKey(localPath))
                return;

            var texture = CreateTexture(File.ReadAllBytes(localPath));
            Cache.TryAdd(localPath, CreateSprite(texture));
            
            Debug.Log($"{localPath} is cached");
        }

        private void TryCacheSprite(string key, byte[] bytes)
        {
            if (Cache.ContainsKey(key))
                return;

            var texture = CreateTexture(bytes);
            Cache.TryAdd(key, CreateSprite(texture));
            Debug.Log($"{key} is cached");
        }
        
        public Sprite CreateSprite(Texture2D spriteTexture) =>
            Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height), Vector2.one / 2f);

        public Texture2D CreateTexture(byte[] bytes)
        {
            var texture2D = new Texture2D(2, 2);
            return !texture2D.LoadImage(bytes) ? null : texture2D;
        }
    }
}