using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Firebase.Database;
using System.Threading.Tasks;
using CCG.Common;
using CCG.Core;
using CCG.Extensions;
using DefaultNamespace;
using Firebase;
using Firebase.Extensions;
using Firebase.Unity.Editor;

public class RealtimeDatabase
{
    private FirebaseDatabase database;
    private DatabaseReference reference;

    private static readonly ConcurrentDictionary<string, List<object>> Cache =
        new ConcurrentDictionary<string, List<object>>();

    private static Task updatedSnapshot;

    private DataSnapshot usersSnapshot;

    public RealtimeDatabase(FirebaseDatabase database)
    {
#if UNITY_EDITOR
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://ccd-game.firebaseio.com/");
#endif
        this.database = database;
        reference = database.RootReference;

        GetUsers();
    }

    void HandleChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        var newMatch = ParseUserMatch((Dictionary<string, object>) args.Snapshot.Value, args.Snapshot.Key);
        var matches = Services.CurrentUser.matches.ToList();
        if (matches.Any(x => x.id == newMatch.id))
            return;
        
        matches.Add(newMatch);
        Services.CurrentUser.matches = matches.ToArray();
    }

    public void UpdateCard(string id, string json)
    {
        reference.Child("cards").Child(id).SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
        {
            if (task.Status == TaskStatus.RanToCompletion)
                Debug.Log($"Card {id} was successfully updated:\n {json}");
        });
    }

    public void UpdateUser(string id, string json)
    {
        reference.Child("users").Child(id).SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
        {
            if (task.Status == TaskStatus.RanToCompletion)
                Debug.Log($"User {id} was successfully updated:\n {json}");
        });
    }

    public void UpdateMatch(string userId, string json)
    {
        reference.Child("users").Child(userId).Child("matches").Push().SetRawJsonValueAsync(json)
            .ContinueWithOnMainThread(task =>
            {
                if (task.Status == TaskStatus.RanToCompletion)
                    Debug.Log($"User {userId} was successfully updated with match info:\n {json}");
            });
    }

    public void UpdateUsersDeck(string userId, string deckId, string json)
    {
        reference.Child("users").Child(userId).Child("decks").Child(deckId).SetRawJsonValueAsync(json)
            .ContinueWithOnMainThread(task =>
            {
                if (task.Status == TaskStatus.RanToCompletion)
                    Debug.Log($"User's {userId} deck {deckId} was successfully updated:\n {json}");
            });
    }

    public void DeleteCard(string id)
    {
        reference.Child("cards").Child(id).RemoveValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.Status == TaskStatus.RanToCompletion)
                Debug.Log($"Card {id} was successfully removed");
        });
    }

    public void GetUsers()
    {
        reference
            .Child("users")
            .GetValueAsync()
            .ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                    Debug.Log($"Couldn't read data about users");
                else if (task.IsCompleted)
                {
                    usersSnapshot = task.Result;

                    var matches = reference.Child("users").Child(Services.CurrentUser.id).Child("matches");
                    matches.ChildAdded += HandleChildAdded;
                }
            });
    }

    public User FindUser(string id)
    {
        var user = usersSnapshot?.Child(id)?.Value;
        var userJson = (Dictionary<string, object>) user;
        if (userJson == null)
            return null;

        var newUser = new User
        {
            id = id,
            login = userJson["login"].ToString(),
            avatar = userJson["avatar"].ToString(),
            lastVisitDate = DateTime.Parse(userJson["last-visit-date"].ToString()),
            matchesCount = int.Parse(userJson["matches-count"].ToString()),
            winRate = float.Parse(userJson["win-rate"].ToString()),
            matches = ParseUserMatches((Dictionary<string, object>) userJson["matches"]),
            decks = ParseUserDecks((Dictionary<string, object>) userJson["decks"]),
        };

        Debug.Log(newUser);

        return newUser;
    }

    private UserDeck[] ParseUserDecks(Dictionary<string, object> decks) =>
        decks.Keys.Select(id => ParseUserDeck((Dictionary<string, object>) decks[id], id)).ToArray();

    private UserDeck ParseUserDeck(Dictionary<string, object> match, string id)
    {
        return new UserDeck
        {
            id = id,
            title = match["title"].ToString(),
            cards = ParseUserCards((List<object>) match["cards"]),
        };
    }

    private string[] ParseUserCards(List<object> cards) =>
        cards.Select(x => x.ToString()).ToArray();

    private UserMatch[] ParseUserMatches(Dictionary<string, object> matches) =>
        matches.Keys.Select(id => ParseUserMatch((Dictionary<string, object>) matches[id], id)).ToArray();

    private UserMatch ParseUserMatch(Dictionary<string, object> match, string id)
    {
        return new UserMatch
        {
            id = id,
            opponentId = match["opponentId"].ToString(),
            result = (MatchResult) int.Parse(match["result"].ToString()),
            status = (MatchStatus) int.Parse(match["status"].ToString()),
        };
    }
}