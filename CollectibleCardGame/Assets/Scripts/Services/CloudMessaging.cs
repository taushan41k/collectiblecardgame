using System;
using System.Collections.Generic;
using System.Linq;
using CCG.Common.Multiplayer;
using CCG.Core;
using DefaultNamespace;
using Firebase.Extensions;
using Firebase.Functions;
using Firebase.Messaging;
using UnityEngine;
using Object = UnityEngine.Object;

namespace CCG.Services
{
    public class CloudMessaging
    {
        private const string RoomId = "roomId";
        private const string Topic = "/topics/rooms";

        private string token;
        private FirebaseFunctions functions;

        private readonly Dictionary<string, string> data = new Dictionary<string, string>();

        public CloudMessaging(FirebaseFunctions functions)
        {
            this.functions = functions;

            FirebaseMessaging.SubscribeAsync(Topic);

            FirebaseMessaging.TokenReceived += TokenReceived;
            FirebaseMessaging.MessageReceived += MessageReceived;
        }

        public void TokenReceived(object sender, TokenReceivedEventArgs token)
        {
            this.token = token.Token;
            Debug.Log("Received Registration Token: " + token.Token);
        }

        public void MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            Debug.Log("Received a new message from: " + e.Message.From);
            if (e.Message.From.Length > 0)
                Debug.Log("from: " + e.Message.From);

            if (e.Message.Data.Count <= 0)
                return;

            Debug.Log("data:");

            foreach (var keyValuePair in e.Message.Data)
            {
                data.Add(keyValuePair.Key, keyValuePair.Value);
                Debug.Log("  " + keyValuePair.Key + ": " + keyValuePair.Value);
            }

            if (!data.ContainsKey("userId"))
                return;

            // if (Common.Services.Authentication.User.UserId == data["userId"])
            //     return;

            var yesNoMenu = Resources.FindObjectsOfTypeAll(typeof(YesNoMenu)).First() as YesNoMenu;
            if (yesNoMenu == null)
                return;
            
            yesNoMenu.gameObject.SetActive(true);
            yesNoMenu.yesButton.onClick.AddListener(() =>
            {
                var match = new UserMatch()
                {
                    opponentId = 2.ToString(),
                    endDate = DateTime.Today,
                    id = Guid.NewGuid().ToString(),
                    result = MatchResult.Lose,
                    status = MatchStatus.Leave
                };
                Common.Services.Database.UpdateMatch(Common.Services.CurrentUser.id, JsonUtility.ToJson(match));
                if (data.ContainsKey(RoomId))
                    new Matchmaking().JoinRoom(data[RoomId]);
            });
        }

        public void SendConnectToRoomNotification(string room)
        {
            var data = new Dictionary<string, object>
            {
                [RoomId] = room,
                ["userId"] = Common.Services.Authentication?.User?.UserId ?? string.Empty
            };

            var function = functions.GetHttpsCallable("sendBattleNotification");
            function.CallAsync(data).ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted && task.Exception != null)
                {
                    foreach (var inner in task.Exception.InnerExceptions)
                    {
                        if (!(inner is FunctionsException exception)) 
                            continue;
                        
                        var code = exception.ErrorCode;
                        var message = exception.Message;

                        Debug.Log($"Error while calling notification. {code}:{message}");
                    }
                }
                else
                {
                    Debug.Log($"!Send notification for battle! {room}");
                }
            });
        }
    }
}