using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CCG.Core;
using Firebase.Auth;
using Firebase.Extensions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CCG.Services
{
    public class FirebaseAuthentication
    {
        private readonly FirebaseAuth auth;

        public static event Action UserRegistered;
        public static event Action UserLoggedIn;
        public static event Action UserLoggedOut;
        
        public FirebaseUser User { get; private set; }

        public FirebaseAuthentication(FirebaseAuth auth)
        {
            this.auth = auth;
            this.auth.StateChanged += AuthStateChanged;
            
            AuthStateChanged(this, null);
        }

        public void Register(string email, string password)
        {
            auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task =>
            {
                GetUser(task, UserRegistered,
                    errorMessage: "CreateUserWithEmailAndPasswordAsync was interrupted: ",
                    successMessage: "Firebase user created successfully:");
            });
        }

        public void Login(string email, string password)
        {
            auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task =>
            {
                GetUser(task, UserLoggedIn,
                    errorMessage: "SignInWithEmailAndPasswordAsync was interrupted: ",
                    successMessage: "Firebase signed in successfully:");
            });
        }

        public void PrintUserInfo() => Debug.LogFormat($"{User.DisplayName} ({User.UserId})");

        public void Logout()
        {
            auth.SignOut();
            
            Application.Quit();
        }

        private void GetUser(Task<FirebaseUser> task, Action callback, string errorMessage, string successMessage)
        {
            if (task.IsCanceled || task.IsFaulted)
            {
                Debug.LogError(errorMessage + task.Exception);
                return;
            }

            User = task.Result;
            Debug.LogFormat($"{successMessage}");
            PrintUserInfo();

            //CreateNewUser();

            callback?.Invoke();
        }

        private void CreateNewUser()
        {
            var avatar = RandomAvatar();
            var user = new User()
            {
                id = User.UserId,
                lastVisitDate = DateTime.Today,
                winRate = 0,
                matchesCount = 0
            };

            var json = JsonUtility.ToJson(user);
            Common.Services.Database.UpdateUser(User.UserId, json);
        }

        private static string RandomAvatar()
        {
            return Enumerable.Range(1, 6).Select(index => $"avatar_{index}").ElementAt(Random.Range(0, 6));
        }

        private void AuthStateChanged(object sender, EventArgs eventArgs) 
        {
            if (auth.CurrentUser == User)
                return;
            
            var signedIn = auth.CurrentUser != null && auth.CurrentUser != User;
            if (!signedIn && User != null)
            {
                Debug.Log("Signed out " + User.UserId);
                UserLoggedOut?.Invoke();
            }

            User = auth.CurrentUser;

            if (!signedIn) 
                return;
            
            Debug.Log("Signed in " + User.UserId);
            UserLoggedIn?.Invoke();
        }

        ~FirebaseAuthentication()
        {
            auth.StateChanged -= AuthStateChanged;
        }
    }
}