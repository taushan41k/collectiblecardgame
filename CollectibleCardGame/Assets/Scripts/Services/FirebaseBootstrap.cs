using System;
using System.Collections.Generic;
using Firebase;
using Firebase.Extensions;

namespace CCG.Services
{
    public static class FirebaseBootstrap
    {
        private static readonly object InitializeMethodsLocker = new object();
        private static readonly List<Action<DependencyStatus>> InitializeMethods = new List<Action<DependencyStatus>>();

        private static DependencyStatus dependencyStatus;
        public static bool Initialized { get; private set; }

        public static void Configure()
        {
            if (Initialized)
                return;
            
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
            {
                lock (InitializeMethodsLocker)
                {
                    dependencyStatus = task.Result;
                    Initialized = true;
                    CallInitializationMethods();
                }
            });
        }

        /// <summary>
        /// Invoke this with a callback to perform some action once the Firebase App is initialized.
        /// If the Firebase App is already initialized, the callback will be invoked immediately.
        /// </summary>
        /// <param name="initialize">The callback to perform once initialized.</param>
        public static void RegisterCallback(Action<DependencyStatus> initialize)
        {
            lock (InitializeMethodsLocker)
            {
                if (Initialized)
                {
                    initialize(dependencyStatus);
                    return;
                }

                InitializeMethods.Add(initialize);
            }
        }

        private static void CallInitializationMethods()
        {
            foreach (var initializeMethod in InitializeMethods)
                initializeMethod(dependencyStatus);

            InitializeMethods.Clear();
        }
    }
}