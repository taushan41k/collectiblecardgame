﻿using System.Linq;
using CCG.Cards;
using CCG.Cards.Model;
using CCG.Common;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

[CustomEditor(typeof(Card))]
public class CardBuilder : Editor
{
    private Card card;
    private VisualElement root;
    private VisualTreeAsset visualTree;

    private Sprite defaultImage;
    private RealtimeDatabase realtimeDatabase;

    private RealtimeDatabase RealtimeDatabase =>
        realtimeDatabase ?? (realtimeDatabase = Services.Database);

    private void OnEnable()
    {
        realtimeDatabase = Services.Database;

        card = (Card) target;
        root = new VisualElement();

        visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/UI/CardEditorTemplate.uxml");
        defaultImage = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Sprites/Card/CardBack.png");
    }

    public override VisualElement CreateInspectorGUI()
    {
        root.Clear();

        visualTree.CloneTree(root);

        BindProperty<TextField>("id", FindSerializedProperty("id"));
        BindProperty<TextField>("title", FindSerializedProperty(0, "text"));
        BindProperty<TextField>("description", FindSerializedProperty(1, "text"));

        BindProperty<IntegerField>("health", FindSerializedProperty(3, "number"));
        BindProperty<IntegerField>("mana", FindSerializedProperty(4, "number"));
        BindProperty<IntegerField>("attack", FindSerializedProperty(5, "number"));

        BindPicker<CardType>("type-picker", FindSerializedProperty("type"));
        var imagePicker = BindPicker<Sprite>("image-picker", FindSerializedProperty(2, "sprite"));

        BindImageProperty(imagePicker);

        FindElement<Button>("update-button").clicked += WriteToDateBase;
        FindElement<Button>("remove-button").clicked += RemoveFromDateBase;

        return root;
    }

    private void BindImageProperty(CallbackEventHandler imagePicker)
    {
        var imageElement = FindElement<VisualElement>("image");
        var artProperty = card.properties.First(x => x.cardElement.name == "Art");
        imageElement.style.backgroundImage = new StyleBackground(ImageTexture());

        imagePicker.RegisterCallback<ChangeEvent<Object>>(x =>
        {
            var image = (Sprite) x.newValue;
            imageElement.style.backgroundImage = new StyleBackground(image.texture);
        });

        Texture2D ImageTexture() =>
            artProperty.sprite != null ? artProperty.sprite.texture : defaultImage.texture;
    }

    private ObjectField FindPicker<T>(string name) where T : Object
    {
        var imagePicker = FindElement<ObjectField>(name);
        imagePicker.objectType = typeof(T);
        return imagePicker;
    }

    private T FindElement<T>(string name) where T : VisualElement =>
        root.Query<T>(name).First();

    private ObjectField BindPicker<T>(string name, SerializedProperty cardProperty) where T : Object
    {
        var picker = FindPicker<T>(name);
        picker.BindProperty(cardProperty);
        return picker;
    }

    private void BindProperty<T>(string name, SerializedProperty cardProperty) where T : VisualElement, IBindable =>
        FindElement<T>(name).BindProperty(cardProperty);

    private SerializedProperty FindSerializedProperty(int index, string name) =>
        FindSerializedProperty("properties", index).FindPropertyRelative(name);

    private SerializedProperty FindSerializedProperty(string name, int index) =>
        FindSerializedProperty(name).GetArrayElementAtIndex(index);

    private SerializedProperty FindSerializedProperty(string name) => serializedObject.FindProperty(name);

    private void WriteToDateBase()
    {
        var json = JsonUtility.ToJson(card.ToDto());
        RealtimeDatabase.UpdateCard(card.id, json);
    }

    private void RemoveFromDateBase()
    {
        RealtimeDatabase.DeleteCard(card.id);

        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID()));
    }
}