using CCG.Common;
using TMPro;

namespace SO.UI
{
    public class UpdateTextFromPhase : UIPropertyUpdater
    {
        public PhaseVariable phase;
        public TMP_Text targetText;
        
        /// <summary>
        /// Use this to update a text UI element based on the target string variable
        /// </summary>
        public override void Raise()
        {
            if (!Services.Game.IsMultiplayer)
                targetText.text = phase.value.name;
        }
        
        public void Raise(string target)
        {
            targetText.text = target;
        }
    }
}