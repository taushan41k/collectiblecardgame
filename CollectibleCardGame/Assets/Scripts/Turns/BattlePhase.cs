using CCG.Common.States;
using CCG.Common.Strategies.Phases;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Battle Phase Player")]
    public class BattlePhase : Phase
    {
        public State battleState;
        public Condition battleValidation;
        
        public override void StartPhase()
        {
            if (isInitialized) 
                return;
            
            forceExit = !battleValidation.IsValid();
            var nextState = forceExit ? null : battleState;
                
            Services.Game.CurrentState = nextState;
            Services.Game.phaseChanged.Raise();
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);

            isInitialized = true;
        }
    }
}