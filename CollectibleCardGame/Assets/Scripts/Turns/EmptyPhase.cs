using System.Collections;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Empty Phase")]
    public class EmptyPhase : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            isInitialized = true;
            
            Services.Game.StartCoroutine(Calculate());
        }

        private IEnumerator Calculate()
        {
            yield return new WaitForSeconds(2f);
          
            var game = Services.Game;
            game.CurrentState = null;
            game.phaseChanged.Raise();
            game.currentPlayer.ResetMana();
            
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);
        }
    }
}