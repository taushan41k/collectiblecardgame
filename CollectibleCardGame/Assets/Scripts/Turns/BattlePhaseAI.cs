using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CCG.Cards;
using UnityEngine;
using Random = System.Random;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Battle Phase AI")]
    public class BattlePhaseAI : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            forceExit = false;
            isInitialized = true;

            Services.Game.StartCoroutine(Calculate());
        }

        private IEnumerator Calculate()
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(2f, 5f));

            var rnd = new Random();
            var gameCurrentPlayer = Services.Game.clientPlayer;
            foreach (var card in gameCurrentPlayer.tableCards.OrderBy(item => rnd.Next()))
            {
                var enemyCard = Services.Game.localPlayer.tableCards
                    .FirstOrDefault(x => !gameCurrentPlayer.AttackingPairs
                        .Select(y => y.enemyCard).Contains(x));

                if (enemyCard == null)
                    continue;

                foreach (var enemyTableCard in Services.Game.localPlayer.tableCards
                    .Where(x => !gameCurrentPlayer.AttackingPairs
                        .Select(y => y.enemyCard).Contains(x))
                    .OrderBy(item => rnd.Next()))
                {
                    if (card.Health - enemyTableCard.Attack < card.Health - enemyCard.Attack)
                        enemyCard = enemyTableCard;
                }

                gameCurrentPlayer.AddAttackingPair(card, enemyCard);
            }

            forceExit = true;
        }
    }
}