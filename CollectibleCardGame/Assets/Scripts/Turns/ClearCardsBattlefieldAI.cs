using System.Collections;
using System.Collections.Generic;
using CCG.Cards;
using CCG.Common.Strategies;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Clear Cards Battlefield Phase AI")]
    public class ClearCardsBattlefieldAI : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            forceExit = false;
            isInitialized = true;
           
            Services.Game.StartCoroutine(Calculate());
        }

        private IEnumerator Calculate()
        {
            yield return new WaitForSeconds(Random.Range(2f, 3f));

            var game = Services.Game;
            var currentPlayer = game.currentPlayer;
            var attackingPairs = new List<(CardController, CardController)>(currentPlayer.AttackingPairs);
            
            foreach (var (playerCard, enemyCard) in attackingPairs)
            {
                if (game.IsMultiplayer)
                    Services.Multiplayer.PlayerReturnsAttackingCard(currentPlayer, playerCard, enemyCard);
                else
                    currentPlayer.RemoveAttackingPair(playerCard, enemyCard);

                playerCard.Highlight(TableCard.DefaultColor);
                enemyCard.Highlight(TableCard.DefaultColor);
            }
            
            game.phaseChanged.Raise();

            forceExit = true;
        }
    }
}