using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Pick Card From Deck Phase")]
    public class PickCardFromDeckPhase : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            var game = Services.Game;
            
            game.PickCardFromDeck();
            
            game.CurrentState = null;
            game.phaseChanged.Raise();
            
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);

            isInitialized = true;
            //forceExit = true;
        }
    }
}