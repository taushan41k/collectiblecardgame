using System.Collections;
using System.Linq;
using CCG.Common.Core;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Battle Resolve Phase AI")]
    public class BattleResolvePhaseAI : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;
            
            forceExit = false;
            isInitialized = true;
            
            Services.Game.StartCoroutine(Calculate());
        }

        private IEnumerator Calculate()
        {
            var game = Services.Game;
            var currentPlayer = game.clientPlayer;
            var enemy = game.localPlayer;

            game.CurrentState = null;
            game.phaseChanged.Raise();

            var attackingCards = currentPlayer.AttackingCards.ToList();
            if (!attackingCards.Any())
                forceExit = true;
            
            yield return new WaitForSeconds(Random.Range(3f, 5f));

            BattleResolver.CalculateSolution(game, currentPlayer, enemy);

            forceExit = true;
        }
    }
}