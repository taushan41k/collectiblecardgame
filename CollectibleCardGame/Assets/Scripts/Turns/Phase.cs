using System;
using UnityEngine;

namespace CCG.Common.Turns
{
    public abstract class Phase : ScriptableObject
    {
        public string name;
        public bool forceExit;
        public bool ai;
        
        [NonSerialized] protected bool isInitialized;

        public virtual bool IsComplete
        {
            get
            {
                if (ai)
                    return true;
                
                if (!forceExit)
                    return false;
                
                forceExit = false;
                return true;
            }
        }
        
        public abstract void StartPhase();

        public virtual void EndPhase()
        {
            if (!isInitialized)
                return;
            
            Services.Game.CurrentState = null;
            isInitialized = false;
            
            Services.Console.RegisterEvent($"{name} just finished!", Color.white);
        }
    }
}