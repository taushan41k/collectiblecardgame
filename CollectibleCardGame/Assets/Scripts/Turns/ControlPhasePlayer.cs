using CCG.Common.States;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Control Phase Player")]
    public class ControlPhasePlayer : Phase
    {
        public State playerControlState;

        public override void StartPhase()
        {
            if (isInitialized)
                return;
            
            Services.Game.CurrentState = playerControlState;
            Services.Game.phaseChanged.Raise();
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);
            isInitialized = true;
        }
    }
}