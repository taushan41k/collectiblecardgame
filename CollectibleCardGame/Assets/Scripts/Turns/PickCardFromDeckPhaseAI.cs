using System.Collections;
using UnityEngine;

namespace CCG.Common.Turns
{
    
    [CreateAssetMenu(menuName = "Turns/Pick Card From Deck Phase AI")]
    public class PickCardFromDeckPhaseAI : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;
            
            isInitialized = true;
            forceExit = false;
            
            Services.Game.StartCoroutine(Calculate());
        }

        private IEnumerator Calculate()
        {
            yield return new WaitForSeconds(Random.Range(2f, 3f));

            var game = Services.Game;
            
            game.clientPlayer.TakeCardFromDeckInHand();
            
            game.CurrentState = null;
            game.phaseChanged.Raise();

            forceExit = true;
        }
    }
}