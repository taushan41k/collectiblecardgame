using System.Collections;
using System.Linq;
using CCG.Cards;
using CCG.Common.Strategies;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Control Phase AI")]
    public class ControlPhaseAI : Phase
    {
        public CardActionType cardDownAction;

        public override void StartPhase()
        {
            if (isInitialized)
                return;
            
            forceExit = false;
            isInitialized = true;

            Services.Game.StartCoroutine(Calculate());
        }

        private IEnumerator Calculate()
        {
            Debug.Log("Waiting " + Time.realtimeSinceStartup);
            yield return new WaitForSeconds(Random.Range(2f, 3f));
            
            var gameCurrentPlayer = Services.Game.clientPlayer;
            var currentPlayerHandCards = gameCurrentPlayer.handCards;
            var topCards = currentPlayerHandCards.OrderBy(x => x.Rate).ToList();
            if (!topCards.Any())
            {
                forceExit = true;
                yield break;
            }

            var cards = topCards.Take(Random.Range(1, currentPlayerHandCards.Count + 1)).ToArray();

            foreach (var card in cards)
            {
                if (gameCurrentPlayer.CanUseCard(card.Model))
                    card.Model.type.Play(card, cardDownAction);
            }

            Services.Game.phaseChanged.Raise();
            forceExit = true;
            Debug.Log("Continue " + Time.realtimeSinceStartup);
        }
    }
}