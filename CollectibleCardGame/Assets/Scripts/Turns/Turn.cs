﻿using System;
using SO;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Turn")]
    public class Turn : ScriptableObject
    {
        public event Action TurnStarted;
        
        public Player player;
        
        public PhaseVariable currentPhase; 
        
        [NonSerialized]
        public int currentPhaseIndex;
        public Phase[] phases;

        public Phase CurrentPhase => phases[currentPhaseIndex];

        public bool Excecute()
        {
            currentPhase.value = CurrentPhase;
            CurrentPhase.StartPhase();
                        
            if (!CurrentPhase.IsComplete) 
                return false;

            CurrentPhase.EndPhase();
            
            currentPhaseIndex++;

            if (currentPhaseIndex <= phases.Length - 1)
                return false;
            
            currentPhaseIndex = 0;

            return true;
        }

        public void EndCurrentPhase() => CurrentPhase.forceExit = true;
        
        public void RaiseStartEvent() => TurnStarted?.Invoke();
    }
}
