using System.Linq;
using CCG.Common.Core;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Battle Resolve Phase")]
    public class BattleResolvePhase : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            isInitialized = true;
            
            var game = Services.Game;
            var currentPlayer = game.currentPlayer;
            var enemy = game.Enemy;

            game.CurrentState = null;
            game.phaseChanged.Raise();
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);

            var attackingCards = currentPlayer.AttackingCards.ToList();
            if (!attackingCards.Any())
                forceExit = true;

            if (game.IsMultiplayer)
                Services.Multiplayer.BroadcastBattleResolution(currentPlayer.photonId, enemy.photonId);
            else
                BattleResolver.CalculateSolution(game, currentPlayer, enemy);
        }
    }
}