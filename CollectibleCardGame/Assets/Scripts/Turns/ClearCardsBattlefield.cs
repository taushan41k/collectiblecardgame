using System.Collections.Generic;
using CCG.Cards;
using CCG.Common.Strategies;
using SO;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Clear Cards Battlefield Phase")]
    public class ClearCardsBattlefield : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            var game = Services.Game;
            var currentPlayer = game.currentPlayer;
            var attackingPairs = new List<(CardController, CardController)>(currentPlayer.AttackingPairs);
            
            foreach (var (playerCard, enemyCard) in attackingPairs)
            {
                if (game.IsMultiplayer)
                    Services.Multiplayer.PlayerReturnsAttackingCard(currentPlayer, playerCard, enemyCard);
                else
                    currentPlayer.RemoveAttackingPair(playerCard, enemyCard);

                playerCard.Highlight(TableCard.DefaultColor);
                enemyCard.Highlight(TableCard.DefaultColor);
            }
            
            game.phaseChanged.Raise();
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);

            isInitialized = true;
            forceExit = true;
        }
    }
}