using System.Linq;
using CCG.Common.Multiplayer;
using UnityEngine;

namespace CCG.Common.Turns
{
    [CreateAssetMenu(menuName = "Turns/Activate Cards For Use Phase")]
    public class ActivateCardsForUsePhase : Phase
    {
        public override void StartPhase()
        {
            if (isInitialized)
                return;

            var game = Services.Game;
            var player = game.CurrentTurn.player;

            foreach (var card in player.tableCards.Where(card => !card.isReadyToUse))
            {
                if (game.IsMultiplayer)
                    Services.Multiplayer.BroadcastPlayerWantsToUseCard(
                        card.Model.instanceId, player.photonId, CardOperation.ActivateForUse);
                else
                    card.isReadyToUse = true;
            }

            game.CurrentState = null;
            game.phaseChanged.Raise();
            Services.Multiplayer?.BroadcastUpdatePhaseText(name);

            isInitialized = true;
        }
    }
}