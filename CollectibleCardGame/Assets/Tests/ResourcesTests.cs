﻿using System.Linq;
using CCG.Cards;
using CCG.Common;
using NUnit.Framework;
using UnityEngine;
using Resources = CCG.Common.Resources;

namespace CCG.Tests
{
    public class ResourcesTests
    {
        private static Resources resources;

        [SetUp]
        public void Initialize() => resources = CCG.Common.Services.Resources;

        [Test]
        public void WhenSpellCard_AndNotNull_ThenHealthPropertyIsZero()
        {
            //Arrange
            var spellCard = resources.allCards.First(x => x.type is Spell);
            var healthProperty = spellCard.properties.First(x => x.cardElement.name == "Health");
            
            //Assert
            Assert.IsTrue(healthProperty.number == 0);
        }
        
        [Test]
        public void WhenSpellCard_AndNotNull_ThenAttackPropertyIsNotZero()
        {
            //Arrange
            var spellCard = resources.allCards.First(x => x.type is Spell);
            var attackProperty = spellCard.properties.First(x => x.cardElement.name == "Attack");
            
            //Assert
            Assert.IsTrue(attackProperty.number != 0);
        }
        
        [Test]
        public void WhenCreatureCard_AndNotNull_ThenHealthPropertyIsNotZero()
        {
            //Arrange
            var creatureCard = resources.allCards.First(x => x.type is Creature);
            var healthProperty = creatureCard.properties.First(x => x.cardElement.name == "Health");
            
            //Assert
            Assert.IsTrue(healthProperty.number != 0);
        } 
        
        [Test]
        public void WhenCreatureCard_AndNotNull_ThenAttackPropertyIsNotZero()
        {
            //Arrange
            var creatureCard = resources.allCards.First(x => x.type is Creature);
            var attackProperty = creatureCard.properties.First(x => x.cardElement.name == "Attack");
            
            //Assert
            Assert.IsTrue(attackProperty.number != 0);
        }
    }
}
