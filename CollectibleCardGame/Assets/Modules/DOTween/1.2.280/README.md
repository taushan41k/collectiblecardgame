*****************************************************************************************
**Bini.3rdParty.DOTween** NuGet Package README
=========================================================================================

Движок для создания tween-анимаций.


*****************************************************************************************
Installation Guide
-----------------------------------------------------------------------------------------

If you're upgrading your project from a version of DOTween older than 1.2.000 (or DOTween Pro older than 1.0.000) please follow these instructions carefully.

1) Import the new version in the same folder as the previous one, overwriting old files. A lot of errors will appear but don't worry
2) Close and reopen Unity (and your project). This is fundamental: skipping this step will cause a bloodbath
3) Open DOTween's Utility Panel (Tools > Demigiant > DOTween Utility Panel) if it doesn't open automatically, then press "Setup DOTween...": this will run the upgrade setup
4) From the Add/Remove Modules panel that opens, activate/deactivate Modules for Unity systems and for external assets (Pro version only)


*****************************************************************************************
Getting Started
-----------------------------------------------------------------------------------------

Сопроводительная документация от разработчика находится в одной папке с этим файлом.


*****************************************************************************************
Helpful Links
-----------------------------------------------------------------------------------------

* Wiki:    https://wiki.magestudio.io/Bini.3rdParty.DOTween
* Website: http://dotween.demigiant.com


*****************************************************************************************
Contributors
-----------------------------------------------------------------------------------------

* %AUTOMATICALLY_POPULATED_LIST%


*****************************************************************************************
Changelog
-----------------------------------------------------------------------------------------

### [1.2.0+1.2.280] - 2020-02-13

Апгрейд до Pro версии, v 1.0.165 (DOTween v 1.2.280). Список изменений - по ссылке http://dotween.demigiant.com/pro.php#changelog.

_________________________________________________________________________________________

### [1.1.0+1.2.250] - 2020-01-22

В пакет добавлен стандартный README.md файл.
