# Bini.3rdParty.MobileConsole

Changelog
-----------------------------------------------------------------------------------------

### [1.2.0+2.0.4] - 2020-06-03

#### Добавить поддержку иврита в MobileConsole [Task TK-502](https://binibambini.atlassian.net/browse/TK-502)

Console.dll changed LogReceiver.LogMessageReceived

From:
```C#
logInfo.time = DateTime.Now.ToString(LogConsoleSettings.Instance.timeFormat);
```

To:
```C#
logInfo.time = DateTime.Now.ToString(LogConsoleSettings.Instance.timeFormat, CultureInfo.InvariantCulture);
```

#### How to do:
1. To change source code use [dnSpy](https://github.com/0xd4d/dnSpy)
2. Chose Console.dll, find LogReceiver class and LogMessageReceived method
3. Right click to method and select 'Edit class c#'
4. Change code
5. Press compile button
6. Export Console.dll using File -> Save Module -> OK

#### Known issues:

| Problem | Solution |
| ------ | ------ |
|UnityEngine.dll not found | drop down UnityEngine.dll to dnSpy window

>more about unity plugins [here](https://docs.unity3d.com/Manual/UsingDLL.html)
