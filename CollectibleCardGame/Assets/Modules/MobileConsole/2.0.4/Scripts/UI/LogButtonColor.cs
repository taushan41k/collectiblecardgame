﻿using UnityEngine;
using UnityEngine.UI;


namespace MobileConsole
{

    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(Image))]
    public class LogButtonColor : MonoBehaviour
    {
        public Color ErrorColor = Color.red;
        public Color StandardColor = Color.white;

        private Button logButton;
        private Image logImage;
        
        private void Start()
        {
            logButton = GetComponent<Button>();
            logImage = GetComponent<Image>();

            if (logButton == null || logImage == null)
                return;

            logButton.onClick.AddListener(() => SetColor(StandardColor, logImage));

            Application.logMessageReceived += ApplicationOnlogMessageReceived;
        }

        private void ApplicationOnlogMessageReceived(string condition, string stacktrace, LogType type)
        {
            if (type == LogType.Error || type == LogType.Assert || type == LogType.Exception)
                SetColor(ErrorColor, logImage);
        }

        private void SetColor(Color color, Image target)
        {
            if (target == null)
                return;

            target.color = color;
        }
    }
}